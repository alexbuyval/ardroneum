#include "ros/ros.h"
#include <tf/tf.h>
#include "std_msgs/String.h"
#include <std_msgs/Empty.h>
#include <std_srvs/Empty.h>
#include <std_msgs/Float32.h>
#include <visual_system/Cross.h>
#include <visual_system/Dashline.h>
#include <navigation/PID.h>
#include <geometry_msgs/Twist.h>
#include <ardrone_autonomy/Navdata.h>
#include <ardrone_autonomy/navdata_magneto.h>
#include <ardrone_autonomy/Ranges.h>
#include <PathFollower.h>

#include <sstream>


#define DISTANCE_BEFORE_ROTATE 2000 //mm


struct USBRangesData
{
	float r_front;
	float r_back;
	float r_left;
	float r_right;
};

enum StateNav {WAIT_START /*0*/, TAKEOFF /*1*/, FLAT_TRIM /*2*/, LIFTING  /*3*/, LIFTING2 /*4*/, DESCENT /*5*/, MOVE_FORWARD /*6*/, MOVE_FORWARD2 /*7*/,
	MOVE_ALONG_LEFT_WALL /*8*/, MOVE_ALONG_LEFT_WALL2 /*9*/, MOVE_ALONG_RIGHT_WALL /*10*/, MOVE_ALONG_RIGHT_WALL2 /*11*/, MOVE_TO_LEFT_WALL /*12*/,
	MOVE_TO_LEFT_WALL2 /*13*/, MOVE_ALONG_FORWARD_WALL /*14*/, MOVE_ALONG_FORWARD_WALL2 /*15*/, ROTATE_LEFT /*16*/, ROTATE_LEFT2 /*17*/, ROTATE_LEFT3 /*18*/,
	ROTATE_LEFT4 /*19*/,  ROTATE_LEFT5  /*20*/, LANDOFF /*21*/, MOVE_ALONG_BACKWARD_WALL2 /*22*/, OVER_OVER_CROSS /*23*/, MOVE_LEFT /*24*/, MOVE_RIGHT /*25*/, KEEP_POSE /*26*/,
	EMB_HOVER /*27*/, EMB_HOVER2 /*28*/, EMB_HOVER3 /*29*/, EMB_HOVER4 /*30*/, EMB_HOVER5 /*31*/, EMB_HOVER6 /*32*/, EMB_HOVER7 /*33*/, EMB_HOVER8 /*34*/,
	EMB_HOVER9 /*35*/, EMB_HOVER10 /*36*/, EMB_HOVER11 /*37*/, EMB_HOVER12 /*38*/, EMB_HOVER13 /*39*/, EMB_HOVER14 /*40*/, EMB_HOVER15 /*41*/, EMB_HOVER16 /*42*/,
	EMB_HOVER17 /*43*/, EMB_HOVER18 /*44*/, EMB_HOVER19 /*45*/, EMB_HOVER20 /*46*/, HOVER_BEFORE_CROSS /*47*/, MOVE_ALONG_2_WALL /*48*/, MOVE_ALONG_2_WALL2  /*49*/,
	MOVE_ALONG_2_WALL3 /*50*/, MOVE_ALONG_2_WALL4 /*51*/, MOVE_ALONG_2_WALL5 /*52*/, MOVE_ALONG_2_WALL6 /*53*/, ROTATE_RIGHT /*54*/, ROTATE_RIGHT2 /*55*/,
	ROTATE_RIGHT3 /*56*/, ROTATE_RIGHT4 /*57*/, ROTATE_RIGHT5, /*58*/ TOUCH_FLOOR /*59*/, LAND_BY_RANGES /*60*/, HOVER_OVER_CROSS/*61*/, LAND_BY_RANGES2 /*62*/,
	MOVE_ALONG_BACKWARD_WALL3 /*63*/, MOVE_FORWARD3 /*64*/,MOVE_FORWARD4 /*65*/, PATH_FOLLOW /*66*/, DASHLINE_FOLLOW /*67*/}; /*Общее число элементов 68*/

enum TypeOfStateNav {tWAIT_START  /*0*/, tTAKEOFF /*1*/, tLIFTING /*2*/, tDESCENT /*3*/, tMOVE_FORWARD /*4*/, tMOVE_ALONG_LEFT_WALL /*5*/, tMOVE_ALONG_RIGHT_WALL /*6*/,
	tMOVE_TO_LEFT_WALL /*7*/, tMOVE_ALONG_FORWARD_WALL /*8*/, tROTATE_LEFT /*9*/, tLANDOFF /*10*/, tMOVE_BACKWARD /*11*/, tHOVER_OVER_CROSS /*12*/, tMOVE_LEFT /*13*/,
	tMOVE_RIGHT /*14*/, tKEEP_POSE /*15*/, tHOVER /*16*/, tHOVER_BEFORE_CROSS /*17*/, tMOVE_ALONG_2_WALL /*18*/, tROTATE_RIGHT /*19*/, tFLAT_TRIM /*20*/, tTOUCH_FLOOR /*21*/,
	tLAND_BY_RANGES /*22*/, tPATH_FOLLOW /*23*/, tDASHLINE_FOLLOW /*24*/}; /*Общее число элементов 25*/

enum Condition {START /*0*/, HEIGHT_REACH /*1*/, FLOOR /*2*/, CROSS /*3*/, HOVER /*4*/, OBSTACLE_AHEAD /*5*/, ROTATE_COMPLETE /*6*/, FREE_LEFT /*7*/, FREE_RIGHT /*8*/,
	LEFT_WALL /*9*/, FRONT_WALL /*10*/, FRONT_FREE /*11*/, DISTANCE_X /*12*/, DISTANCE_Y /*13*/, CENTER_OF_CROSS /*14*/, TIME /*15*/, BEFORE_CROSS /*16*/, LAND_X /*17*/,
    LAND_RANGES /*18*/, OBSTACLE_AHEAD_FAR /*19*/, LAND /*20*/}; /*Общее число элементов 21*/

enum Action {NONE /*0*/, TOGGLECAM /*1*/, CALIBRATE_COMPASS /*2*/, FLATTRIM /*3*/, CALC_X_CROSS_POSE /*4*/, SET_START_ANGLE /*5*/, CALC_DIST_BACK /*6*/}; /*Общее число элементов 6*/

struct ElementOfFlightProgram {
	StateNav state;
	Condition condition_transition; //условие перехода в следующее состояние
	float ahead_angle;
	float distance;
	float duration;
	float long_speed;
	int height;
	Action action; //действие при входе в состояние
	StateNav new_state; //следующее состояние
};

class PID
{
	ros::NodeHandle *nh;
	ros::Publisher pid_pub;
	ros::Time last_time;

	double dState;    // Last position input
	double iState;    // Integrator state
	double iMax, iMin;  // Maximum and minimum allowable integrator state
	double oMax, oMin; //Maximum and minimum allowable output
	double    iGain,        // integral gain
	pGain,        // proportional gain
	dGain;         // derivative gain



public:
	PID(ros::NodeHandle* _nh, std::string _name)
{

		nh=_nh;

		if (ros::param::get("~pGain_"+_name, pGain))
		{
			ROS_INFO("pGain_%s =%f", _name.c_str(), (float)pGain);
		}
		else
		{
			ROS_ERROR("Can't get %s parameter", std::string("pGain_"+_name).c_str());
		}

		if (ros::param::get("~iGain_"+_name, iGain))
		{
			ROS_INFO("iGain_%s =%f", _name.c_str(), (float)iGain);
		}
		else
		{
			ROS_ERROR("Can't get %s parameter", std::string("iGain_"+_name).c_str());
		}

		if (ros::param::get("~dGain_"+_name, dGain))
		{
			ROS_INFO("dGain_%s =%f", _name.c_str(),(float) dGain);
		}
		else
		{
			ROS_ERROR("Can't get %s parameter", std::string("dGain_"+_name).c_str());
		}

		if (ros::param::get("~iMax_"+_name, iMax))
		{
			ROS_INFO("iMax_%s =%f", _name.c_str(),(float)iMax);
		}
		else
		{
			ROS_ERROR("Can't get %s parameter", std::string("iMax_"+_name).c_str());
		}

		if (ros::param::get("~iMin_"+_name, iMin))
		{
			ROS_INFO("iMin_%s =%f", _name.c_str(),(float)iMin);
		}
		else
		{
			ROS_ERROR("Can't get %s parameter", std::string("iMin_"+_name).c_str());
		}

		if (ros::param::get("~oMax_"+_name, oMax))
		{
			ROS_INFO("oMax_%s =%f", _name.c_str(),(float)oMax);
		}
		else
		{
			ROS_ERROR("Can't get %s parameter", std::string("oMax_"+_name).c_str());
		}

		if (ros::param::get("~oMin_"+_name, oMin))
		{
			ROS_INFO("oMin_%s =%f", _name.c_str(),(float)oMin);
		}
		else
		{
			ROS_ERROR("Can't get %s parameter", std::string("oMin_"+_name).c_str());
		}


		pid_pub = nh->advertise<navigation::PID>("PID/"+_name, 1);

		dState = 0;
		iState = 0;

};

	double computeDeltaTime(const ros::Time& t)
	{
		double dt = 0.0;

		if(!last_time.isZero())
		{
			dt = (t - last_time).toSec();
		}

		last_time = t;

		return dt;
	}




	double UpdatePID(const ros::Time& t, double error, double derror, double cur_value, double des_value)
	{
		double pTerm, dTerm, iTerm, res;

		double dt = computeDeltaTime(t);

		pTerm = pGain * error;    // calculate the proportional term
		iState += error*dt;          // calculate the integral state with appropriate limiting

		if (iState > iMax)
			iState = iMax;
		else if (iState < iMin)
			iState = iMin;
		iTerm = iGain * iState;    // calculate the integral term
        dTerm = dGain * derror;
		dState = cur_value;
		res = pTerm + iTerm + dTerm;

		if (res > oMax)
			res = oMax;
		else if (res < oMin)
			res = oMin;


		navigation::PID msg;

		msg.header.stamp = ros::Time::now();

		msg.res=res;
		msg.pTerm=pTerm;
		msg.iTerm=iTerm;
		msg.dTerm=dTerm;

		msg.cur_value=cur_value;
		msg.des_value=des_value;
		msg.err=error;
		msg.derr = derror;

		pid_pub.publish(msg);

		return res;
	}

	void ResetIntState()
	{
		iState=0;
	}

};

class ArDroneNav
{
	ros::NodeHandle nh_;
	ros::Subscriber ranges_sub; // USB Data
	ros::Subscriber navi_cross; // Cross
	ros::Subscriber dashline_sub; // Dashline
    ros::Subscriber manualControl_sub; //Manual control
    ros::Subscriber autoControl_sub; //Manual control
	ros::Publisher takeoff_pub; // Takeoff
	ros::Publisher land_pub;    // Land
	ros::Publisher cmdvel_pub;
	ros::Publisher pose_pub;
	ros::Publisher target_pose_pub;
	ros::Subscriber start_sub;
	ros::Subscriber check_sub;
	ros::Subscriber navi_sub;
    ros::Subscriber land_sub;
	ros::Subscriber navi_mag_sub;
	ros::ServiceClient client_tc; //клиент переключения камеры
	ros::ServiceClient client_calib_compass; //клиент калибровки компаса
	ros::ServiceClient client_ft; //клиент вызова flattrim
	std_srvs::Empty srv_tc;
	ros::Time initTime, LastTime, LastLogTime, curTime;
	std_msgs::Empty empty_msg;
	geometry_msgs::Twist cmd_vel_msg;
	tf::Pose pose_;
	PathFollower *pathFollower;

	float dx, dy;
	StateNav state_nav;
	ElementOfFlightProgram *FlightProgram;
	int maxCountElements; //max count of elements flight program
    bool Conditions[21]; //количество элементов равно количеству значений в перечислении Condition
	TypeOfStateNav Types[68]; //количество элементов равно количеству значений в перечислении StateNav
	float startangle; //Угол поворота в начальный момент времени
	bool angle_estimated;
	bool dist_start; // Начало работы дальномеров
	int cnt_dist;
	float target_angle, cur_angle, norm_diff_angle, target_dist, target_time;
	double cur_long_speed_hover_cross; //текущая продольная скорость поиска креста
	double cur_long_speed;//текущая продольная скорость
	int cur_height; //текущая высота
	double x_land; //рассчитанная точка посадки по оси x при обнаружении креста
	bool flagStab;//флаг стабилизации по дальномерам при парении перед крестом
	double dist_backward;//расстояние на которое надо отлететь назад в м;

	double left_range_cross, right_range_cross, offset_cross, x_pose_cross; //показания дальномеров в момент условия CROSS и смещение креста на картинке в см, а также позиция по x комптера

	USBRangesData RangesDate_cur;
	USBRangesData RangesDate_prev;
	ros::Time prevCrossTime, prevNavDataTime;
	int CountCross;
	bool CrossIsFound;
	ardrone_autonomy::Navdata LastNavDataMsg;
	ardrone_autonomy::navdata_magneto LastNavMagMsg;
	float x,y;
	double dt;
	int count;
    bool modeHover;
    ros::Time startTimeHover;


	//last coordinates cross
	float x_cross,y_cross;
	ros::Time lastObservCross;

	//last parameters of dashline
    float dashline_slope, dashline_dist, lastDashlineDist;
    bool dashline_straightPath;
	ros::Time lastObservDashline;

	//PID controllers
	PID * pid_Height; //регулятор высоты
	PID * pid_Direction; //регулятор направления
	PID * pid_Lateral; //регулятор расстояния до стены
	PID * pid_CrossX; //регулятор посадки на крест по оси Х
	PID * pid_CrossY; //регулятор посадки на крест по оси Y
	PID * pid_PoseX; //регулятор удержания позиции по оси X
	PID * pid_PoseY; //регулятор удержания позиции по оси Y

    bool manualControl;
    ros::Time lastManualControl;

	//parameters
	double longitudial_speed, lateral_speed, angular_speed, longitudial_speed_cross;
	int HeightOfFly; //высота полета в мм
	int HeightOfCross; //высота полета над крестом мм
	double Kp_Cross; //пропорциаональный коэф. регулятора посадки на крест
	double Kp_HeightOfFly; //пропорциаональный коэф. регулятора высоты
	double Ki_HeightOfFly; //интегральный коэф. регулятора высоты
	double Kp_lat_stab; //пропорциаональный коэф. регулятора боковой уст.
	double Ki_lat_stab; //интегральный коэф. регулятора боковой уст.
	double limit_lat_stab; //лимит боковой стабилизации
	double limit_height_stab; //лимит стабилизации по высоте
	bool DebugOutput; //флаг вывода отладочных сообщений
	int numberOfFlyProgram; //номер полетной программы
	double distToWall; //расстояние до стены, которое нужное держать в см
	double freeDist; //расстояние, которое служит сигналом того, что кончилась стена в см
	double aheadDist; //расстояние перед дроном ближе, которого уже лететь не нужно в см
	double aheadDistFar;//рассчтояния для условия срабатывания на дальние преграды
	double timeTouchFloor; //время, которое отводиться для касания пола
	double long_speed_hover_cross; //продольная скорость при поиске креста
    double long_speed_along_straight_path; //продольная скорость на прямом участке
	bool useGyro; //если истина, то используются только показания гироскопа
	bool useRangeForCross; //если истина, то после получения креста будет рассчитываться смещение и будет стабилизация по дальномерам
	double land_front, lateral_shift; //показание переднего дальномера и боковое смещение относительно центра, чтобы обеспечить посадку в центр круга в см
	double aheadDistBeforeTurn; //показания переднего дальномера перед поворотом
	double distToLand; //расстояние до посадки от фронтальной стены в см
    bool isGazeboSimulation; //симуляция или нет
    bool useHoverStabilization; //использовать встроенный режим парения для поперечной стабилизации

public:
	ArDroneNav()
{
		ROS_INFO("Init");
		ranges_sub = nh_.subscribe("ardrone/ranges", 1, &ArDroneNav::USBData, this); // USB Data
		takeoff_pub = nh_.advertise<std_msgs::Empty>("/ardrone/takeoff", 1);
		land_pub = nh_.advertise<std_msgs::Empty>("/ardrone/land", 1);
		cmdvel_pub = nh_.advertise<geometry_msgs::Twist>("/cmd_vel", 1);
		pose_pub = nh_.advertise<geometry_msgs::PoseStamped>("/pose",1);
		target_pose_pub = nh_.advertise<geometry_msgs::PoseStamped>("/target_pose",1);
		start_sub = nh_.subscribe("/start", 1, &ArDroneNav::startCb, this);
		check_sub = nh_.subscribe("/check", 1, &ArDroneNav::checkCb, this);
		navi_sub = nh_.subscribe("/ardrone/navdata", 1, &ArDroneNav::naviCb, this);
		navi_mag_sub = nh_.subscribe("ardrone/navdata_magneto", 1, &ArDroneNav::navi_magCb, this);
		navi_cross = nh_.subscribe("/cross", 1, &ArDroneNav::crossCb, this);
		dashline_sub = nh_.subscribe("/dashline", 1, &ArDroneNav::DashlineCb, this);
        land_sub = nh_.subscribe("/ardrone/land", 1, &ArDroneNav::LandCb, this);
        manualControl_sub = nh_.subscribe("/manual_control", 1, &ArDroneNav::manualControlCb, this);
        autoControl_sub = nh_.subscribe("/auto_control", 1, &ArDroneNav::autoControlCb, this);

		client_tc = nh_.serviceClient<std_srvs::Empty>("/ardrone/togglecam");
		client_calib_compass = nh_.serviceClient<std_srvs::Empty>("ardrone/magcalib");
		client_ft = nh_.serviceClient<std_srvs::Empty>("ardrone/flattrim");

		pid_Height = new PID(&nh_, "Height");
		pid_Direction = new PID(&nh_, "Direction");
		pid_Lateral = new PID(&nh_, "Lateral");
		pid_CrossX = new PID(&nh_, "CrossX");
		pid_CrossY = new PID(&nh_, "CrossY");
		pid_PoseX = new PID(&nh_, "PoseX");
		pid_PoseY = new PID(&nh_, "PoseY");

        manualControl = false;

		//load parameters
		longitudial_speed = (ros::param::get("~longitudial_speed", longitudial_speed)) ? longitudial_speed : 0.03;
		longitudial_speed_cross = (ros::param::get("~longitudial_speed_cross", longitudial_speed_cross)) ? longitudial_speed_cross : 0.03;
		lateral_speed = (ros::param::get("~lateral_speed", lateral_speed)) ? lateral_speed : 0.03;
		angular_speed = (ros::param::get("~angular_speed", angular_speed)) ? angular_speed : 0.3;
		HeightOfFly = (ros::param::get("~HeightOfFly", HeightOfFly)) ? HeightOfFly : 700;
		HeightOfCross = (ros::param::get("~HeightOfCross", HeightOfCross)) ? HeightOfCross : 1800;
		DebugOutput = (ros::param::get("~DebugOutput", DebugOutput)) ? DebugOutput : true;
		numberOfFlyProgram = (ros::param::get("~numberOfFlyProgram", numberOfFlyProgram)) ? numberOfFlyProgram : 0;
		distToWall = (ros::param::get("~distToWall", distToWall)) ? distToWall : 100;
		freeDist = (ros::param::get("~freeDist", freeDist)) ? freeDist : 400;
		aheadDist = (ros::param::get("~ahedDist", aheadDist)) ? aheadDist : 200;
		aheadDistFar = (ros::param::get("~ahedDistFar", aheadDistFar)) ? aheadDistFar : 300;
		timeTouchFloor = (ros::param::get("~timeTouchFloor", timeTouchFloor)) ? timeTouchFloor : 1.0;
		long_speed_hover_cross = (ros::param::get("~long_speed_hover_cross", long_speed_hover_cross)) ? long_speed_hover_cross : -0.04;
        long_speed_along_straight_path = (ros::param::get("~long_speed_along_straight_path", long_speed_along_straight_path)) ? long_speed_along_straight_path : 0.08;
		useGyro = (ros::param::get("~useGyro", useGyro)) ? useGyro : false;
        isGazeboSimulation = (ros::param::get("~isGazeboSimulation", isGazeboSimulation)) ? isGazeboSimulation : false;
        useHoverStabilization = (ros::param::get("~useHoverStabilization", useHoverStabilization)) ? useHoverStabilization: false;
		land_front = (ros::param::get("~land_front", land_front)) ? land_front : 200.0;
		lateral_shift = (ros::param::get("~lateral_shift", lateral_shift)) ? lateral_shift : 50.0;
		useRangeForCross = (ros::param::get("~useRangeForCross", useRangeForCross)) ? useRangeForCross : false;
		aheadDistBeforeTurn = (ros::param::get("~aheadDistBeforeTurn", aheadDistBeforeTurn)) ? aheadDistBeforeTurn : 300.0;
		distToLand = (ros::param::get("~distToLand", distToLand)) ? distToLand: 500.0;

		ROS_INFO("longitudial speed =%.2f", longitudial_speed);
		ROS_INFO("HeightOfFly =%d", HeightOfFly);
		ROS_INFO("angular_speed =%.2f", angular_speed);
		ROS_INFO("land_front =%.2f", land_front);
		ROS_INFO("lateral_shift =%.2f", lateral_shift);

		dx = 0;
		state_nav = WAIT_START;
		startangle = 0.0;
		cur_angle =0;
		angle_estimated = false;
		cnt_dist = 0;
		target_angle = 0;
		RangesDate_cur.r_front = 0;
		RangesDate_cur.r_back = 0;
		RangesDate_cur.r_left = 0;
		RangesDate_cur.r_right = 0;
		RangesDate_prev.r_front = 0;
		RangesDate_prev.r_back = 0;
		RangesDate_prev.r_left = 0;
		RangesDate_prev.r_right = 0;
		CountCross = 0;
		left_range_cross = 0;
		right_range_cross = 0;
		offset_cross = 0;
		x_pose_cross = 0;
		cur_long_speed_hover_cross = -long_speed_hover_cross;
		dist_backward = 0;
        modeHover = false;

		flagStab = false;

		x=0;y=0;dt=0;
		CrossIsFound = false;
		InitFilghtProgram();
		InitConditions();
		ResetIntegErrors();
		count=0;

		pathFollower = new PathFollower(&target_pose_pub);
        //pathFollower->generateEightPath();
        pathFollower->generateSimplePath();



}

	~ArDroneNav()
	{

	}


	void InitFilghtProgram()
	{

		Types[WAIT_START] = tWAIT_START;
		Types[TAKEOFF] = tTAKEOFF;
		Types[FLAT_TRIM] = tFLAT_TRIM;
		Types[LIFTING] = tLIFTING;
		Types[LIFTING2] = tLIFTING;
		Types[DESCENT] = tDESCENT;
		Types[TOUCH_FLOOR] = tTOUCH_FLOOR;
		Types[MOVE_FORWARD] = tMOVE_FORWARD;
		Types[MOVE_FORWARD2] = tMOVE_FORWARD;
		Types[MOVE_FORWARD3] = tMOVE_FORWARD;
		Types[MOVE_FORWARD4] = tMOVE_FORWARD;
		Types[MOVE_ALONG_LEFT_WALL] = tMOVE_ALONG_LEFT_WALL;
		Types[MOVE_ALONG_RIGHT_WALL] = tMOVE_ALONG_RIGHT_WALL;
		Types[MOVE_ALONG_RIGHT_WALL2] = tMOVE_ALONG_RIGHT_WALL;
		Types[MOVE_ALONG_LEFT_WALL2] = tMOVE_ALONG_LEFT_WALL;
		Types[MOVE_ALONG_FORWARD_WALL] = tMOVE_ALONG_FORWARD_WALL;
		Types[MOVE_ALONG_FORWARD_WALL2] = tMOVE_ALONG_FORWARD_WALL;
		Types[MOVE_TO_LEFT_WALL] = tMOVE_TO_LEFT_WALL;
		Types[MOVE_TO_LEFT_WALL2] = tMOVE_TO_LEFT_WALL;
		Types[ROTATE_LEFT] = tROTATE_LEFT;
		Types[ROTATE_LEFT2] = tROTATE_LEFT;
		Types[ROTATE_LEFT3] = tROTATE_LEFT;
		Types[ROTATE_LEFT4] = tROTATE_LEFT;
		Types[ROTATE_LEFT5] = tROTATE_LEFT;
		Types[ROTATE_RIGHT] = tROTATE_RIGHT;
		Types[ROTATE_RIGHT2] = tROTATE_RIGHT;
		Types[ROTATE_RIGHT3] = tROTATE_RIGHT;
		Types[ROTATE_RIGHT4] = tROTATE_RIGHT;
		Types[ROTATE_RIGHT5] = tROTATE_RIGHT;
		Types[LANDOFF] = tLANDOFF;
		Types[MOVE_ALONG_BACKWARD_WALL2] = tMOVE_BACKWARD;
		Types[MOVE_ALONG_BACKWARD_WALL3] = tMOVE_BACKWARD;
		Types[MOVE_LEFT] = tMOVE_LEFT;
		Types[MOVE_RIGHT] = tMOVE_RIGHT;
		Types[HOVER_OVER_CROSS] = tHOVER_OVER_CROSS;
		Types[KEEP_POSE] = tKEEP_POSE;
		Types[EMB_HOVER] = tHOVER;
		Types[EMB_HOVER2] = tHOVER;
		Types[EMB_HOVER3] = tHOVER;
		Types[EMB_HOVER4] = tHOVER;
		Types[EMB_HOVER5] = tHOVER;
		Types[EMB_HOVER6] = tHOVER;
		Types[EMB_HOVER7] = tHOVER;
		Types[EMB_HOVER8] = tHOVER;
		Types[EMB_HOVER9] = tHOVER;
		Types[EMB_HOVER10] = tHOVER;
		Types[EMB_HOVER11] = tHOVER;
		Types[EMB_HOVER12] = tHOVER;
		Types[EMB_HOVER13] = tHOVER;
		Types[EMB_HOVER14] = tHOVER;
		Types[EMB_HOVER15] = tHOVER;
		Types[EMB_HOVER16] = tHOVER;
		Types[EMB_HOVER17] = tHOVER;
		Types[EMB_HOVER18] = tHOVER;
		Types[EMB_HOVER19] = tHOVER;
		Types[EMB_HOVER20] = tHOVER;
		Types[HOVER_BEFORE_CROSS] = tHOVER_BEFORE_CROSS;
		Types[MOVE_ALONG_2_WALL] = tMOVE_ALONG_2_WALL;
		Types[MOVE_ALONG_2_WALL2] = tMOVE_ALONG_2_WALL;
		Types[MOVE_ALONG_2_WALL3] = tMOVE_ALONG_2_WALL;
		Types[MOVE_ALONG_2_WALL4] = tMOVE_ALONG_2_WALL;
		Types[MOVE_ALONG_2_WALL5] = tMOVE_ALONG_2_WALL;
		Types[MOVE_ALONG_2_WALL6] = tMOVE_ALONG_2_WALL;
		Types[LAND_BY_RANGES] = tLAND_BY_RANGES;
		Types[LAND_BY_RANGES2] = tLAND_BY_RANGES;
		Types[PATH_FOLLOW] = tPATH_FOLLOW;
		Types[DASHLINE_FOLLOW] = tDASHLINE_FOLLOW;

		int i=0;
		switch (numberOfFlyProgram)
		{

		case 0: //основной вариант
			maxCountElements = 41;
			FlightProgram = new ElementOfFlightProgram[maxCountElements];
			FlightProgram[i].state = WAIT_START; FlightProgram[i].condition_transition=START; FlightProgram[i].new_state=FLAT_TRIM; FlightProgram[i].action = NONE; FlightProgram[i].ahead_angle = 0; FlightProgram[i].height = HeightOfFly;  i++;
			FlightProgram[i].state = FLAT_TRIM; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=TAKEOFF; FlightProgram[i].duration = 1.0; FlightProgram[i].action = FLATTRIM; FlightProgram[i].ahead_angle = 0; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = TAKEOFF; FlightProgram[i].condition_transition=HOVER; FlightProgram[i].new_state=EMB_HOVER; FlightProgram[i].action = NONE; FlightProgram[i].ahead_angle = 0; FlightProgram[i].height = HeightOfFly; i++;
			if (useGyro == true){
				FlightProgram[i].state = EMB_HOVER; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;
			}else{
				FlightProgram[i].state = EMB_HOVER; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 4.5; FlightProgram[i].action = CALIBRATE_COMPASS; FlightProgram[i].height = HeightOfFly; i++;
			}
			FlightProgram[i].state = LIFTING; FlightProgram[i].condition_transition=HEIGHT_REACH; FlightProgram[i].new_state=MOVE_ALONG_2_WALL; FlightProgram[i].ahead_angle = 0; FlightProgram[i].action = SET_START_ANGLE; FlightProgram[i].height = HeightOfFly; i++;
			//5
			//двигаемся вдоль стен до свободного пространства слева
			FlightProgram[i].state = MOVE_ALONG_2_WALL; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=MOVE_ALONG_2_WALL2; FlightProgram[i].ahead_angle = 0; FlightProgram[i].distance = 5; FlightProgram[i].long_speed=longitudial_speed;  FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = MOVE_ALONG_2_WALL2; FlightProgram[i].condition_transition=FREE_LEFT; FlightProgram[i].new_state=EMB_HOVER2; FlightProgram[i].ahead_angle = 0; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfFly; i++;

			FlightProgram[i].state = EMB_HOVER2; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=ROTATE_LEFT; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 0.7; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = ROTATE_LEFT; FlightProgram[i].condition_transition=ROTATE_COMPLETE; FlightProgram[i].new_state=MOVE_ALONG_RIGHT_WALL; FlightProgram[i].ahead_angle = -80; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = MOVE_ALONG_RIGHT_WALL; FlightProgram[i].condition_transition=OBSTACLE_AHEAD_FAR; FlightProgram[i].new_state=EMB_HOVER3; FlightProgram[i].ahead_angle = -90; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = EMB_HOVER3; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=ROTATE_LEFT2; FlightProgram[i].ahead_angle = -90; FlightProgram[i].duration = 0.7; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = ROTATE_LEFT2; FlightProgram[i].condition_transition=ROTATE_COMPLETE; FlightProgram[i].new_state=MOVE_ALONG_RIGHT_WALL2; FlightProgram[i].ahead_angle = 180; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = MOVE_ALONG_RIGHT_WALL2; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=MOVE_ALONG_2_WALL3; FlightProgram[i].distance = 2; FlightProgram[i].ahead_angle = 180; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfFly; i++;
			//здесь нужно добавить движение вдоль 2-х стен с условием по дистанции метров 15 (чтобы исключить ложные срабатывание креста и дальномера)
			FlightProgram[i].state = MOVE_ALONG_2_WALL3; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=MOVE_ALONG_2_WALL4; FlightProgram[i].ahead_angle = 180; FlightProgram[i].distance = 3;  FlightProgram[i].long_speed=longitudial_speed; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = MOVE_ALONG_2_WALL4; FlightProgram[i].condition_transition=CROSS; FlightProgram[i].new_state=EMB_HOVER4; FlightProgram[i].ahead_angle = 180; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfCross; i++;
			FlightProgram[i].state = MOVE_ALONG_2_WALL4; FlightProgram[i].condition_transition=OBSTACLE_AHEAD; FlightProgram[i].new_state=EMB_HOVER4; FlightProgram[i].ahead_angle = 180; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfCross; i++;
			//16
			//ищем крест, стабилизируемя по нему и приземляемся
			FlightProgram[i].state = EMB_HOVER4; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=HOVER_BEFORE_CROSS; FlightProgram[i].ahead_angle = 180; FlightProgram[i].duration = 0.5; FlightProgram[i].action = CALC_X_CROSS_POSE; FlightProgram[i].height = HeightOfCross; i++;
			FlightProgram[i].state = HOVER_BEFORE_CROSS; FlightProgram[i].condition_transition=BEFORE_CROSS; FlightProgram[i].new_state=EMB_HOVER5; FlightProgram[i].ahead_angle = 180; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfCross; i++;
			FlightProgram[i].state = EMB_HOVER5; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=DESCENT; FlightProgram[i].ahead_angle = 180; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = DESCENT; FlightProgram[i].condition_transition=FLOOR; FlightProgram[i].new_state=EMB_HOVER6; FlightProgram[i].ahead_angle = 180; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = DESCENT; FlightProgram[i].condition_transition=LAND_X; FlightProgram[i].new_state=EMB_HOVER6; FlightProgram[i].ahead_angle = 180; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = EMB_HOVER6; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=TOUCH_FLOOR; FlightProgram[i].ahead_angle = 180; FlightProgram[i].duration = 0.7; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = TOUCH_FLOOR; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING2; FlightProgram[i].ahead_angle = 180; FlightProgram[i].duration = timeTouchFloor; FlightProgram[i].height = HeightOfFly; i++;
			//23
			//взлетаем и летим задним ходом
			FlightProgram[i].state = LIFTING2; FlightProgram[i].condition_transition=HEIGHT_REACH; FlightProgram[i].new_state=MOVE_ALONG_BACKWARD_WALL2; FlightProgram[i].ahead_angle = 180; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = MOVE_ALONG_BACKWARD_WALL2; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=EMB_HOVER10; FlightProgram[i].ahead_angle = 180; FlightProgram[i].distance = 2; FlightProgram[i].long_speed=longitudial_speed; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = EMB_HOVER10; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=ROTATE_LEFT3; FlightProgram[i].ahead_angle = 180; FlightProgram[i].duration = 0.7; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;
			//разворачиваемся и летим назад
			FlightProgram[i].state = ROTATE_LEFT3; FlightProgram[i].condition_transition=ROTATE_COMPLETE; FlightProgram[i].new_state=MOVE_ALONG_2_WALL5; FlightProgram[i].ahead_angle = 0; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = MOVE_ALONG_2_WALL5; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=MOVE_ALONG_2_WALL6; FlightProgram[i].ahead_angle = 0; FlightProgram[i].distance = 5; FlightProgram[i].long_speed=longitudial_speed; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = MOVE_ALONG_2_WALL6; FlightProgram[i].condition_transition=FREE_RIGHT; FlightProgram[i].new_state=EMB_HOVER7; FlightProgram[i].ahead_angle = 0; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = EMB_HOVER7; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=ROTATE_RIGHT; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 0.7; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;
			//28
			//проходим последний поворот и летим вдоль стен
			FlightProgram[i].state = ROTATE_RIGHT; FlightProgram[i].condition_transition=ROTATE_COMPLETE; FlightProgram[i].new_state=MOVE_ALONG_LEFT_WALL; FlightProgram[i].ahead_angle = 90; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = MOVE_ALONG_LEFT_WALL; FlightProgram[i].condition_transition=OBSTACLE_AHEAD_FAR; FlightProgram[i].new_state=EMB_HOVER8; FlightProgram[i].ahead_angle = 90; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = EMB_HOVER8; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=ROTATE_RIGHT2; FlightProgram[i].ahead_angle = 90; FlightProgram[i].duration = 0.7; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = ROTATE_RIGHT2; FlightProgram[i].condition_transition=ROTATE_COMPLETE; FlightProgram[i].new_state=MOVE_ALONG_LEFT_WALL2; FlightProgram[i].ahead_angle = 180; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = MOVE_ALONG_LEFT_WALL2; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=LAND_BY_RANGES; FlightProgram[i].distance = 3; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].ahead_angle = 180; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = LAND_BY_RANGES; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=LAND_BY_RANGES2; FlightProgram[i].ahead_angle = 180; FlightProgram[i].distance = 5; FlightProgram[i].long_speed=longitudial_speed; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = LAND_BY_RANGES2; FlightProgram[i].condition_transition=LAND_RANGES; FlightProgram[i].new_state=EMB_HOVER9; FlightProgram[i].ahead_angle = 180; FlightProgram[i].action = NONE; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfFly; i++;

			FlightProgram[i].state = EMB_HOVER9; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=EMB_HOVER11; FlightProgram[i].ahead_angle = 180; FlightProgram[i].duration = 0.7; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = EMB_HOVER11; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=MOVE_ALONG_BACKWARD_WALL3; FlightProgram[i].ahead_angle = 180; FlightProgram[i].duration = 0.1; FlightProgram[i].action = CALC_DIST_BACK; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = MOVE_ALONG_BACKWARD_WALL3; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=EMB_HOVER12; FlightProgram[i].ahead_angle = 180; FlightProgram[i].distance = dist_backward; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = EMB_HOVER12; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LANDOFF; FlightProgram[i].ahead_angle = 180; FlightProgram[i].duration = 0.7; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;


			break;

		case 1: // Пролет вдоль 2-х стен до свободного пространства, поворот, пролет по правой стене до препятствия, поворот и пролет по дистанции

			maxCountElements = 14;
			FlightProgram = new ElementOfFlightProgram[maxCountElements];
			FlightProgram[i].state = WAIT_START; FlightProgram[i].condition_transition=START; FlightProgram[i].new_state=FLAT_TRIM; FlightProgram[i].action = NONE; FlightProgram[i].ahead_angle = 0; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = FLAT_TRIM; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=TAKEOFF; FlightProgram[i].duration = 1.0; FlightProgram[i].action = FLATTRIM; FlightProgram[i].ahead_angle = 0; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = TAKEOFF; FlightProgram[i].condition_transition=HOVER; FlightProgram[i].new_state=EMB_HOVER; FlightProgram[i].action = NONE; FlightProgram[i].ahead_angle = 0;  i++;
			if (useGyro == true){
				FlightProgram[i].state = EMB_HOVER; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;
			}else{
				FlightProgram[i].state = EMB_HOVER; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 4.5; FlightProgram[i].action = CALIBRATE_COMPASS; FlightProgram[i].height = HeightOfFly; i++;
			}
			FlightProgram[i].state = LIFTING; FlightProgram[i].condition_transition=HEIGHT_REACH; FlightProgram[i].new_state=MOVE_ALONG_2_WALL; FlightProgram[i].ahead_angle = 0; FlightProgram[i].action = SET_START_ANGLE; FlightProgram[i].height = HeightOfFly; i++;
			//5
			//двигаемся вдоль стен до свободного пространства слева
			FlightProgram[i].state = MOVE_ALONG_2_WALL; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=MOVE_ALONG_2_WALL2; FlightProgram[i].ahead_angle = 0; FlightProgram[i].distance = 27; FlightProgram[i].long_speed=longitudial_speed; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = MOVE_ALONG_2_WALL2; FlightProgram[i].condition_transition=FREE_LEFT; FlightProgram[i].new_state=EMB_HOVER2; FlightProgram[i].ahead_angle = 0; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfFly; i++;

			FlightProgram[i].state = EMB_HOVER2; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=ROTATE_LEFT; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 0.7; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = ROTATE_LEFT; FlightProgram[i].condition_transition=ROTATE_COMPLETE; FlightProgram[i].new_state=MOVE_ALONG_RIGHT_WALL; FlightProgram[i].ahead_angle = 80; FlightProgram[i].height = HeightOfFly;  i++;
			FlightProgram[i].state = MOVE_ALONG_RIGHT_WALL; FlightProgram[i].condition_transition=OBSTACLE_AHEAD_FAR; FlightProgram[i].new_state=EMB_HOVER3; FlightProgram[i].ahead_angle = 90; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = EMB_HOVER3; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=ROTATE_LEFT2; FlightProgram[i].ahead_angle = 90; FlightProgram[i].duration = 0.7; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = ROTATE_LEFT2; FlightProgram[i].condition_transition=ROTATE_COMPLETE; FlightProgram[i].new_state=MOVE_ALONG_RIGHT_WALL2; FlightProgram[i].ahead_angle = 180; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = MOVE_ALONG_RIGHT_WALL2; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=MOVE_ALONG_2_WALL3; FlightProgram[i].distance = 2; FlightProgram[i].ahead_angle = 180; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = MOVE_ALONG_2_WALL3; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=LANDOFF; FlightProgram[i].ahead_angle = 180; FlightProgram[i].distance = 10;  FlightProgram[i].long_speed=longitudial_speed; FlightProgram[i].height = HeightOfFly; i++;
			break;

		case 2: //Взлет, пролет вдоль 2-х стен, посадка на крест, взлет, разворот, пролет вдоль 2-х стен по дистанции
			maxCountElements = 19;
			FlightProgram = new ElementOfFlightProgram[maxCountElements];
			FlightProgram[i].state = WAIT_START; FlightProgram[i].condition_transition=START; FlightProgram[i].new_state=FLAT_TRIM; FlightProgram[i].action = NONE; FlightProgram[i].ahead_angle = 0; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = FLAT_TRIM; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=TAKEOFF; FlightProgram[i].duration = 1.0; FlightProgram[i].action = FLATTRIM; FlightProgram[i].ahead_angle = 0; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = TAKEOFF; FlightProgram[i].condition_transition=HOVER; FlightProgram[i].new_state=EMB_HOVER; FlightProgram[i].action = NONE; FlightProgram[i].ahead_angle = 0; FlightProgram[i].height = HeightOfFly; i++;
			if (useGyro == true){
				FlightProgram[i].state = EMB_HOVER; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;
			}else{
				FlightProgram[i].state = EMB_HOVER; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 4.5; FlightProgram[i].action = CALIBRATE_COMPASS; FlightProgram[i].height = HeightOfFly; i++;
			}
			FlightProgram[i].state = LIFTING; FlightProgram[i].condition_transition=HEIGHT_REACH; FlightProgram[i].new_state=MOVE_ALONG_2_WALL4; FlightProgram[i].ahead_angle = 0; FlightProgram[i].action = SET_START_ANGLE; FlightProgram[i].height = HeightOfFly; i++;
			//5
			FlightProgram[i].state = MOVE_ALONG_2_WALL4; FlightProgram[i].condition_transition=CROSS; FlightProgram[i].new_state=EMB_HOVER4; FlightProgram[i].ahead_angle = 0; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfCross; i++;
			FlightProgram[i].state = MOVE_ALONG_2_WALL4; FlightProgram[i].condition_transition=OBSTACLE_AHEAD; FlightProgram[i].new_state=EMB_HOVER4; FlightProgram[i].ahead_angle = 0; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfCross; i++;
			//ищем крест, стабилизируемя по нему и приземляемся
			FlightProgram[i].state = EMB_HOVER4; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=HOVER_BEFORE_CROSS; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 0.5; FlightProgram[i].action = CALC_X_CROSS_POSE; FlightProgram[i].height = HeightOfCross; i++;
			FlightProgram[i].state = HOVER_BEFORE_CROSS; FlightProgram[i].condition_transition=BEFORE_CROSS; FlightProgram[i].new_state=EMB_HOVER5; FlightProgram[i].ahead_angle = 0; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfCross; i++;
			FlightProgram[i].state = EMB_HOVER5; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=DESCENT; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfCross; i++;
			FlightProgram[i].state = DESCENT; FlightProgram[i].condition_transition=FLOOR; FlightProgram[i].new_state=EMB_HOVER6; FlightProgram[i].ahead_angle = 0; FlightProgram[i].height = HeightOfCross; i++;
			FlightProgram[i].state = DESCENT; FlightProgram[i].condition_transition=LAND_X; FlightProgram[i].new_state=EMB_HOVER6; FlightProgram[i].ahead_angle = 0; FlightProgram[i].height = HeightOfCross; i++;
			FlightProgram[i].state = EMB_HOVER6; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=TOUCH_FLOOR; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 0.7; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfCross; i++;
			FlightProgram[i].state = TOUCH_FLOOR; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING2; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = timeTouchFloor; FlightProgram[i].height = HeightOfCross; i++;
			//взлетаем и летим задним ходом
			FlightProgram[i].state = LIFTING2; FlightProgram[i].condition_transition=HEIGHT_REACH; FlightProgram[i].new_state=MOVE_ALONG_BACKWARD_WALL2; FlightProgram[i].ahead_angle = 180; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = MOVE_ALONG_BACKWARD_WALL2; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=EMB_HOVER10; FlightProgram[i].ahead_angle = 180; FlightProgram[i].distance = 3; FlightProgram[i].long_speed=longitudial_speed; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = EMB_HOVER10; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=ROTATE_LEFT3; FlightProgram[i].ahead_angle = 180; FlightProgram[i].duration = 0.7; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;
			//разворачиваемся и летим назад
			FlightProgram[i].state = ROTATE_LEFT3; FlightProgram[i].condition_transition=ROTATE_COMPLETE; FlightProgram[i].new_state=MOVE_ALONG_2_WALL5; FlightProgram[i].ahead_angle = 170; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = MOVE_ALONG_2_WALL5; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=LANDOFF; FlightProgram[i].ahead_angle = 180; FlightProgram[i].distance = 5; FlightProgram[i].long_speed=longitudial_speed; FlightProgram[i].height = HeightOfFly; i++;


			break;
		case 3: //Взлет пролет вдоль 2-х стен до своб. пространства справо, поворот на право, пролет вдоль 2-х стен и посадка по дальномерам
			maxCountElements = 18;
			FlightProgram = new ElementOfFlightProgram[maxCountElements];
			FlightProgram[i].state = WAIT_START; FlightProgram[i].condition_transition=START; FlightProgram[i].new_state=FLAT_TRIM; FlightProgram[i].action = NONE; FlightProgram[i].ahead_angle = 0; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = FLAT_TRIM; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=TAKEOFF; FlightProgram[i].duration = 1.0; FlightProgram[i].action = FLATTRIM; FlightProgram[i].ahead_angle = 0; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = TAKEOFF; FlightProgram[i].condition_transition=HOVER; FlightProgram[i].new_state=EMB_HOVER; FlightProgram[i].action = NONE; FlightProgram[i].ahead_angle = 0; FlightProgram[i].height = HeightOfFly; i++;
			if (useGyro == true){
				FlightProgram[i].state = EMB_HOVER; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;
			}else{
				FlightProgram[i].state = EMB_HOVER; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 4.5; FlightProgram[i].action = CALIBRATE_COMPASS; FlightProgram[i].height = HeightOfFly; i++;
			}
			FlightProgram[i].state = LIFTING; FlightProgram[i].condition_transition=HEIGHT_REACH; FlightProgram[i].new_state=MOVE_ALONG_2_WALL6; FlightProgram[i].ahead_angle = 0; FlightProgram[i].action = SET_START_ANGLE; FlightProgram[i].height = HeightOfFly; i++;
			//5
			FlightProgram[i].state = MOVE_ALONG_2_WALL6; FlightProgram[i].condition_transition=FREE_RIGHT; FlightProgram[i].new_state=EMB_HOVER7; FlightProgram[i].ahead_angle = 0; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = EMB_HOVER7; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=ROTATE_RIGHT; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 0.7; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;
			//проходим последний поворот и летим вдоль стен
			FlightProgram[i].state = ROTATE_RIGHT; FlightProgram[i].condition_transition=ROTATE_COMPLETE; FlightProgram[i].new_state=MOVE_ALONG_LEFT_WALL; FlightProgram[i].ahead_angle = 90; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = MOVE_ALONG_LEFT_WALL; FlightProgram[i].condition_transition=OBSTACLE_AHEAD_FAR; FlightProgram[i].new_state=EMB_HOVER8; FlightProgram[i].ahead_angle = 90; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = EMB_HOVER8; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=ROTATE_RIGHT2; FlightProgram[i].ahead_angle = 90; FlightProgram[i].duration = 0.7; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = ROTATE_RIGHT2; FlightProgram[i].condition_transition=ROTATE_COMPLETE; FlightProgram[i].new_state=MOVE_ALONG_LEFT_WALL2; FlightProgram[i].ahead_angle = 180; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = MOVE_ALONG_LEFT_WALL2; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=LAND_BY_RANGES; FlightProgram[i].distance = 3; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].ahead_angle = 180; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = LAND_BY_RANGES; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=LAND_BY_RANGES2; FlightProgram[i].ahead_angle = 180; FlightProgram[i].distance = 20; FlightProgram[i].long_speed=longitudial_speed; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = LAND_BY_RANGES2; FlightProgram[i].condition_transition=LAND_RANGES; FlightProgram[i].new_state=EMB_HOVER9; FlightProgram[i].ahead_angle = 180; FlightProgram[i].action = NONE; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfFly; i++;

			FlightProgram[i].state = EMB_HOVER9; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=EMB_HOVER11; FlightProgram[i].ahead_angle = 180; FlightProgram[i].duration = 0.7; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = EMB_HOVER11; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=MOVE_ALONG_BACKWARD_WALL3; FlightProgram[i].ahead_angle = 180; FlightProgram[i].duration = 0.1; FlightProgram[i].action = CALC_DIST_BACK; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = MOVE_ALONG_BACKWARD_WALL3; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=EMB_HOVER12; FlightProgram[i].ahead_angle = 180; FlightProgram[i].distance = dist_backward; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = EMB_HOVER12; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LANDOFF; FlightProgram[i].ahead_angle = 180; FlightProgram[i].duration = 0.7; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;

			break;
		case 4: //пролет вдоль 2-х стен и посадка по дальномерам
			maxCountElements = 11;
			FlightProgram = new ElementOfFlightProgram[maxCountElements];
			FlightProgram[i].state = WAIT_START; FlightProgram[i].condition_transition=START; FlightProgram[i].new_state=FLAT_TRIM; FlightProgram[i].action = NONE; FlightProgram[i].ahead_angle = 0;  i++;
			FlightProgram[i].state = FLAT_TRIM; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=TAKEOFF; FlightProgram[i].duration = 1.0; FlightProgram[i].action = FLATTRIM; FlightProgram[i].ahead_angle = 0; i++;
			FlightProgram[i].state = TAKEOFF; FlightProgram[i].condition_transition=HOVER; FlightProgram[i].new_state=EMB_HOVER; FlightProgram[i].action = NONE; FlightProgram[i].ahead_angle = 0; i++;
			if (useGyro == true){
				FlightProgram[i].state = EMB_HOVER; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; i++;
			}else{
				FlightProgram[i].state = EMB_HOVER; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 4.5; FlightProgram[i].action = CALIBRATE_COMPASS; i++;
			}
			FlightProgram[i].state = LIFTING; FlightProgram[i].condition_transition=HEIGHT_REACH; FlightProgram[i].new_state=LAND_BY_RANGES; FlightProgram[i].ahead_angle = 0; FlightProgram[i].action = SET_START_ANGLE; i++;
			//5
			FlightProgram[i].state = LAND_BY_RANGES; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=LAND_BY_RANGES2; FlightProgram[i].ahead_angle = 0; FlightProgram[i].distance = 10; FlightProgram[i].long_speed=longitudial_speed; i++;
			FlightProgram[i].state = LAND_BY_RANGES2; FlightProgram[i].condition_transition=LAND_RANGES; FlightProgram[i].new_state=EMB_HOVER9; FlightProgram[i].ahead_angle = 0; FlightProgram[i].action = NONE; FlightProgram[i].long_speed=longitudial_speed_cross; i++;

			FlightProgram[i].state = EMB_HOVER9; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=EMB_HOVER11; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 0.7; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = EMB_HOVER11; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=MOVE_ALONG_BACKWARD_WALL3; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 0.1; FlightProgram[i].action = CALC_DIST_BACK; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = MOVE_ALONG_BACKWARD_WALL3; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=EMB_HOVER12; FlightProgram[i].ahead_angle = 0; FlightProgram[i].distance = dist_backward; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = EMB_HOVER12; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LANDOFF; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 0.7; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;

			break;


		case 5: //Для тестирования в коридоре. Взлет, пролет вперед до своб. прост. слева, потом опять пролет вперед, стабилизация на крест, снижение на крест, подъем, разворот на 180 и посадка
			maxCountElements = 16;
			FlightProgram = new ElementOfFlightProgram[maxCountElements];
			FlightProgram[i].state = WAIT_START; FlightProgram[i].condition_transition=START; FlightProgram[i].new_state=FLAT_TRIM; FlightProgram[i].action = NONE; FlightProgram[i].ahead_angle = 0;  i++;
			FlightProgram[i].state = FLAT_TRIM; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=TAKEOFF; FlightProgram[i].duration = 1.0; FlightProgram[i].action = FLATTRIM; FlightProgram[i].ahead_angle = 0; i++;
			FlightProgram[i].state = TAKEOFF; FlightProgram[i].condition_transition=HOVER; FlightProgram[i].new_state=EMB_HOVER; FlightProgram[i].action = NONE; FlightProgram[i].ahead_angle = 0; i++;
			if (useGyro == true){
				FlightProgram[i].state = EMB_HOVER; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; i++;
			}else{
				FlightProgram[i].state = EMB_HOVER; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 4.5; FlightProgram[i].action = CALIBRATE_COMPASS; i++;
			}
			FlightProgram[i].state = LIFTING; FlightProgram[i].condition_transition=HEIGHT_REACH; FlightProgram[i].new_state=EMB_HOVER6; FlightProgram[i].ahead_angle = 0; FlightProgram[i].action = SET_START_ANGLE; FlightProgram[i].long_speed=0; i++;
			FlightProgram[i].state = EMB_HOVER6; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=MOVE_ALONG_2_WALL2; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 0.7; FlightProgram[i].action = NONE; FlightProgram[i].long_speed=0; i++;
			FlightProgram[i].state = MOVE_ALONG_2_WALL2; FlightProgram[i].condition_transition=FREE_LEFT; FlightProgram[i].new_state=MOVE_FORWARD; FlightProgram[i].ahead_angle = 0; FlightProgram[i].long_speed=longitudial_speed; i++;
			FlightProgram[i].state = MOVE_FORWARD; FlightProgram[i].condition_transition=CROSS; FlightProgram[i].new_state=EMB_HOVER3; FlightProgram[i].ahead_angle = 0; FlightProgram[i].long_speed=longitudial_speed_cross; i++;
			FlightProgram[i].state = MOVE_FORWARD; FlightProgram[i].condition_transition=OBSTACLE_AHEAD; FlightProgram[i].new_state=EMB_HOVER3; FlightProgram[i].ahead_angle = 0; FlightProgram[i].action = NONE; FlightProgram[i].long_speed=longitudial_speed_cross; i++;
			FlightProgram[i].state = EMB_HOVER3; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=HOVER_BEFORE_CROSS; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 0.5; FlightProgram[i].action = CALC_X_CROSS_POSE; i++;
			FlightProgram[i].state = HOVER_BEFORE_CROSS; FlightProgram[i].condition_transition=BEFORE_CROSS; FlightProgram[i].new_state=EMB_HOVER4; FlightProgram[i].ahead_angle = 0; FlightProgram[i].action = NONE; i++;
			FlightProgram[i].state = EMB_HOVER4; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=DESCENT; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; i++;
			FlightProgram[i].state = DESCENT; FlightProgram[i].condition_transition=FLOOR; FlightProgram[i].new_state=EMB_HOVER7; FlightProgram[i].ahead_angle = 0; i++;
			FlightProgram[i].state = DESCENT; FlightProgram[i].condition_transition=LAND_X; FlightProgram[i].new_state=EMB_HOVER7; FlightProgram[i].ahead_angle = 0; i++;
			FlightProgram[i].state = EMB_HOVER7; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=TOUCH_FLOOR; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 0.7; FlightProgram[i].action = NONE; i++;
			FlightProgram[i].state = TOUCH_FLOOR; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LANDOFF; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 2.0; i++;
			//FlightProgram[i].state = LIFTING2; FlightProgram[i].condition_transition=HEIGHT_REACH; FlightProgram[i].new_state=ROTATE_LEFT3; FlightProgram[i].ahead_angle = 0; i++;
			//FlightProgram[i].state = ROTATE_LEFT3; FlightProgram[i].condition_transition=ROTATE_COMPLETE; FlightProgram[i].new_state=MOVE_FORWARD2; FlightProgram[i].ahead_angle = 170; i++;
			//FlightProgram[i].state = MOVE_FORWARD2; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=LANDOFF; FlightProgram[i].ahead_angle = 180; FlightProgram[i].distance = 1;  FlightProgram[i].long_speed=longitudial_speed; i++;
			break;

		case 6: //Для тестирования в коридоре. Взлет, пролет вперед по дистанции,зависание, разворот на 180 градусов, пролёт вперёд, стабилизация на крест, снижение на крест, подъем, разворот на 180 и посадка
			maxCountElements = 23;
			FlightProgram = new ElementOfFlightProgram[maxCountElements];
			FlightProgram[i].state = WAIT_START; FlightProgram[i].condition_transition=START; FlightProgram[i].new_state=FLAT_TRIM; FlightProgram[i].action = NONE; FlightProgram[i].ahead_angle = 0;  i++;
			FlightProgram[i].state = FLAT_TRIM; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=TAKEOFF; FlightProgram[i].duration = 1.0; FlightProgram[i].action = FLATTRIM; FlightProgram[i].ahead_angle = 0; i++;
			FlightProgram[i].state = TAKEOFF; FlightProgram[i].condition_transition=HOVER; FlightProgram[i].new_state=EMB_HOVER; FlightProgram[i].action = NONE; FlightProgram[i].ahead_angle = 0; i++;
			if (useGyro == true){
				FlightProgram[i].state = EMB_HOVER; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; i++;
			}else{
				FlightProgram[i].state = EMB_HOVER; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 4.5; FlightProgram[i].action = CALIBRATE_COMPASS; i++;
			}
			FlightProgram[i].state = LIFTING; FlightProgram[i].condition_transition=HEIGHT_REACH; FlightProgram[i].new_state=EMB_HOVER6; FlightProgram[i].ahead_angle = 0; FlightProgram[i].action = SET_START_ANGLE; FlightProgram[i].long_speed=0; i++;
			FlightProgram[i].state = EMB_HOVER6; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=MOVE_ALONG_2_WALL; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 0.7; FlightProgram[i].action = NONE; FlightProgram[i].long_speed=0; i++;
			FlightProgram[i].state = MOVE_ALONG_2_WALL; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=EMB_HOVER2; FlightProgram[i].ahead_angle = 0; FlightProgram[i].distance = 5; FlightProgram[i].long_speed=longitudial_speed; i++;
			FlightProgram[i].state = EMB_HOVER2; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=ROTATE_LEFT; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 0.7; FlightProgram[i].action = NONE; i++;
			FlightProgram[i].state = ROTATE_LEFT; FlightProgram[i].condition_transition=ROTATE_COMPLETE; FlightProgram[i].new_state=EMB_HOVER5; FlightProgram[i].ahead_angle = 170; i++;
			FlightProgram[i].state = EMB_HOVER5; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=MOVE_ALONG_2_WALL2; FlightProgram[i].ahead_angle = 180; FlightProgram[i].duration = 0.5; FlightProgram[i].action = NONE; i++;
			FlightProgram[i].state = MOVE_ALONG_2_WALL2; FlightProgram[i].condition_transition=FREE_LEFT; FlightProgram[i].new_state=MOVE_FORWARD; FlightProgram[i].ahead_angle = 180; FlightProgram[i].long_speed=longitudial_speed; i++;
			FlightProgram[i].state = MOVE_FORWARD; FlightProgram[i].condition_transition=CROSS; FlightProgram[i].new_state=EMB_HOVER3; FlightProgram[i].ahead_angle = 180; FlightProgram[i].long_speed=longitudial_speed_cross; i++;
			FlightProgram[i].state = MOVE_FORWARD; FlightProgram[i].condition_transition=OBSTACLE_AHEAD; FlightProgram[i].new_state=EMB_HOVER3; FlightProgram[i].ahead_angle = 180; FlightProgram[i].action = NONE; FlightProgram[i].long_speed=longitudial_speed_cross; i++;
			FlightProgram[i].state = EMB_HOVER3; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=HOVER_BEFORE_CROSS; FlightProgram[i].ahead_angle = 180; FlightProgram[i].duration = 0.5; FlightProgram[i].action = CALC_X_CROSS_POSE; i++;
			FlightProgram[i].state = HOVER_BEFORE_CROSS; FlightProgram[i].condition_transition=BEFORE_CROSS; FlightProgram[i].new_state=EMB_HOVER4; FlightProgram[i].ahead_angle = 180; FlightProgram[i].action = NONE; i++;
			FlightProgram[i].state = EMB_HOVER4; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=DESCENT; FlightProgram[i].ahead_angle = 180; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; i++;
			FlightProgram[i].state = DESCENT; FlightProgram[i].condition_transition=FLOOR; FlightProgram[i].new_state=EMB_HOVER7; FlightProgram[i].ahead_angle = 180; i++;
			FlightProgram[i].state = DESCENT; FlightProgram[i].condition_transition=LAND_X; FlightProgram[i].new_state=EMB_HOVER7; FlightProgram[i].ahead_angle = 180; i++;
			FlightProgram[i].state = EMB_HOVER7; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=TOUCH_FLOOR; FlightProgram[i].ahead_angle = 180; FlightProgram[i].duration = 0.7; FlightProgram[i].action = NONE; i++;
			FlightProgram[i].state = TOUCH_FLOOR; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING2; FlightProgram[i].ahead_angle = 180; FlightProgram[i].duration = 2.5; i++;
			FlightProgram[i].state = LIFTING2; FlightProgram[i].condition_transition=HEIGHT_REACH; FlightProgram[i].new_state=ROTATE_LEFT3; FlightProgram[i].ahead_angle = 180; i++;
			FlightProgram[i].state = ROTATE_LEFT3; FlightProgram[i].condition_transition=ROTATE_COMPLETE; FlightProgram[i].new_state=MOVE_FORWARD2; FlightProgram[i].ahead_angle = 350; i++;
			FlightProgram[i].state = MOVE_FORWARD2; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=LANDOFF; FlightProgram[i].ahead_angle = 0; FlightProgram[i].distance = 1;  FlightProgram[i].long_speed=longitudial_speed; i++;
			break;

		case 7: //Для тестирования в коридоре. Пролёт вперёд/назад на 15 метров бесконечное число раз
			maxCountElements = 14;
			FlightProgram = new ElementOfFlightProgram[maxCountElements];
			FlightProgram[i].state = WAIT_START; FlightProgram[i].condition_transition=START; FlightProgram[i].new_state=FLAT_TRIM; FlightProgram[i].action = NONE; FlightProgram[i].ahead_angle = 0;  i++;
			FlightProgram[i].state = FLAT_TRIM; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=TAKEOFF; FlightProgram[i].duration = 1.0; FlightProgram[i].action = FLATTRIM; FlightProgram[i].ahead_angle = 0; i++;
			FlightProgram[i].state = TAKEOFF; FlightProgram[i].condition_transition=HOVER; FlightProgram[i].new_state=EMB_HOVER; FlightProgram[i].action = NONE; FlightProgram[i].ahead_angle = 0; i++;
			if (useGyro == true){
				FlightProgram[i].state = EMB_HOVER; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; i++;
			}else{
				FlightProgram[i].state = EMB_HOVER; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 4.5; FlightProgram[i].action = CALIBRATE_COMPASS; i++;
			}
			FlightProgram[i].state = LIFTING; FlightProgram[i].condition_transition=HEIGHT_REACH; FlightProgram[i].new_state=EMB_HOVER2; FlightProgram[i].ahead_angle = 0; FlightProgram[i].action = SET_START_ANGLE; i++;
			FlightProgram[i].state = EMB_HOVER2; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=MOVE_ALONG_2_WALL; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; i++;
			FlightProgram[i].state = MOVE_ALONG_2_WALL; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=EMB_HOVER3; FlightProgram[i].ahead_angle = 0; FlightProgram[i].distance = 15; i++;
			FlightProgram[i].state = EMB_HOVER3; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=ROTATE_LEFT; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; i++;
			FlightProgram[i].state = ROTATE_LEFT; FlightProgram[i].condition_transition=ROTATE_COMPLETE; FlightProgram[i].new_state=EMB_HOVER4; FlightProgram[i].ahead_angle = 170; i++;
			FlightProgram[i].state = EMB_HOVER4; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=MOVE_ALONG_2_WALL2; FlightProgram[i].ahead_angle = 180; FlightProgram[i].duration = 0.5; FlightProgram[i].action = NONE; i++;
			FlightProgram[i].state = MOVE_ALONG_2_WALL2; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=EMB_HOVER5; FlightProgram[i].ahead_angle = 180; FlightProgram[i].distance = 15; i++;
			FlightProgram[i].state = EMB_HOVER5; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=ROTATE_RIGHT; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; i++;
			FlightProgram[i].state = ROTATE_RIGHT; FlightProgram[i].condition_transition=ROTATE_COMPLETE; FlightProgram[i].new_state=EMB_HOVER6; FlightProgram[i].ahead_angle = 10; i++;
			FlightProgram[i].state = EMB_HOVER6; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=EMB_HOVER2; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; i++;
			break;
		case 8: //основной вариант для Газебо (другие углы)
			maxCountElements = 41;
			FlightProgram = new ElementOfFlightProgram[maxCountElements];
			FlightProgram[i].state = WAIT_START; FlightProgram[i].condition_transition=START; FlightProgram[i].new_state=FLAT_TRIM; FlightProgram[i].action = NONE; FlightProgram[i].ahead_angle = 0; FlightProgram[i].height = HeightOfFly;  i++;
			FlightProgram[i].state = FLAT_TRIM; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=TAKEOFF; FlightProgram[i].duration = 1.0; FlightProgram[i].action = FLATTRIM; FlightProgram[i].ahead_angle = 0; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = TAKEOFF; FlightProgram[i].condition_transition=HOVER; FlightProgram[i].new_state=EMB_HOVER; FlightProgram[i].action = NONE; FlightProgram[i].ahead_angle = 0; FlightProgram[i].height = HeightOfFly; i++;
			if (useGyro == true){
				FlightProgram[i].state = EMB_HOVER; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;
			}else{
				FlightProgram[i].state = EMB_HOVER; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 4.5; FlightProgram[i].action = CALIBRATE_COMPASS; FlightProgram[i].height = HeightOfFly; i++;
			}
			FlightProgram[i].state = LIFTING; FlightProgram[i].condition_transition=HEIGHT_REACH; FlightProgram[i].new_state=MOVE_ALONG_2_WALL; FlightProgram[i].ahead_angle = 0; FlightProgram[i].action = SET_START_ANGLE; FlightProgram[i].height = HeightOfFly; i++;
			//5
			//двигаемся вдоль стен до свободного пространства слева
			FlightProgram[i].state = MOVE_ALONG_2_WALL; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=MOVE_ALONG_2_WALL2; FlightProgram[i].ahead_angle = 0; FlightProgram[i].distance = 22; FlightProgram[i].long_speed=longitudial_speed;  FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = MOVE_ALONG_2_WALL2; FlightProgram[i].condition_transition=FREE_LEFT; FlightProgram[i].new_state=EMB_HOVER2; FlightProgram[i].ahead_angle = 0; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfFly; i++;

			FlightProgram[i].state = EMB_HOVER2; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=ROTATE_LEFT; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 0.7; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = ROTATE_LEFT; FlightProgram[i].condition_transition=ROTATE_COMPLETE; FlightProgram[i].new_state=MOVE_ALONG_RIGHT_WALL; FlightProgram[i].ahead_angle = 80; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = MOVE_ALONG_RIGHT_WALL; FlightProgram[i].condition_transition=OBSTACLE_AHEAD_FAR; FlightProgram[i].new_state=EMB_HOVER3; FlightProgram[i].ahead_angle = 90; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = EMB_HOVER3; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=ROTATE_LEFT2; FlightProgram[i].ahead_angle = -90; FlightProgram[i].duration = 0.7; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = ROTATE_LEFT2; FlightProgram[i].condition_transition=ROTATE_COMPLETE; FlightProgram[i].new_state=MOVE_ALONG_RIGHT_WALL2; FlightProgram[i].ahead_angle = 180; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = MOVE_ALONG_RIGHT_WALL2; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=MOVE_ALONG_2_WALL3; FlightProgram[i].distance = 2; FlightProgram[i].ahead_angle = 180; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfFly; i++;
			//здесь нужно добавить движение вдоль 2-х стен с условием по дистанции метров 15 (чтобы исключить ложные срабатывание креста и дальномера)
			FlightProgram[i].state = MOVE_ALONG_2_WALL3; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=MOVE_ALONG_2_WALL4; FlightProgram[i].ahead_angle = 180; FlightProgram[i].distance = 18;  FlightProgram[i].long_speed=longitudial_speed; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = MOVE_ALONG_2_WALL4; FlightProgram[i].condition_transition=CROSS; FlightProgram[i].new_state=EMB_HOVER4; FlightProgram[i].ahead_angle = 180; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfCross; i++;
			FlightProgram[i].state = MOVE_ALONG_2_WALL4; FlightProgram[i].condition_transition=OBSTACLE_AHEAD; FlightProgram[i].new_state=EMB_HOVER4; FlightProgram[i].ahead_angle = 180; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfCross; i++;
			//16
			//ищем крест, стабилизируемя по нему и приземляемся
			FlightProgram[i].state = EMB_HOVER4; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=HOVER_BEFORE_CROSS; FlightProgram[i].ahead_angle = 180; FlightProgram[i].duration = 0.5; FlightProgram[i].action = CALC_X_CROSS_POSE; FlightProgram[i].height = HeightOfCross; i++;
			FlightProgram[i].state = HOVER_BEFORE_CROSS; FlightProgram[i].condition_transition=BEFORE_CROSS; FlightProgram[i].new_state=EMB_HOVER5; FlightProgram[i].ahead_angle = 180; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfCross; i++;
			FlightProgram[i].state = EMB_HOVER5; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=DESCENT; FlightProgram[i].ahead_angle = 180; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = DESCENT; FlightProgram[i].condition_transition=FLOOR; FlightProgram[i].new_state=EMB_HOVER6; FlightProgram[i].ahead_angle = 180; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = DESCENT; FlightProgram[i].condition_transition=LAND_X; FlightProgram[i].new_state=EMB_HOVER6; FlightProgram[i].ahead_angle = 180; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = EMB_HOVER6; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=TOUCH_FLOOR; FlightProgram[i].ahead_angle = 180; FlightProgram[i].duration = 0.7; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = TOUCH_FLOOR; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING2; FlightProgram[i].ahead_angle = 180; FlightProgram[i].duration = timeTouchFloor; FlightProgram[i].height = HeightOfFly; i++;
			//23
			//взлетаем, разворачиваемся и летим назад
			FlightProgram[i].state = LIFTING2; FlightProgram[i].condition_transition=HEIGHT_REACH; FlightProgram[i].new_state=MOVE_ALONG_BACKWARD_WALL2; FlightProgram[i].ahead_angle = 180; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = MOVE_ALONG_BACKWARD_WALL2; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=EMB_HOVER10; FlightProgram[i].ahead_angle = 180; FlightProgram[i].distance = 3; FlightProgram[i].long_speed=longitudial_speed; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = EMB_HOVER10; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=ROTATE_LEFT3; FlightProgram[i].ahead_angle = 180; FlightProgram[i].duration = 0.7; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;

			FlightProgram[i].state = ROTATE_LEFT3; FlightProgram[i].condition_transition=ROTATE_COMPLETE; FlightProgram[i].new_state=MOVE_ALONG_2_WALL5; FlightProgram[i].ahead_angle = 350; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = MOVE_ALONG_2_WALL5; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=MOVE_ALONG_2_WALL6; FlightProgram[i].ahead_angle = 0; FlightProgram[i].distance = 15; FlightProgram[i].long_speed=longitudial_speed; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = MOVE_ALONG_2_WALL6; FlightProgram[i].condition_transition=FREE_RIGHT; FlightProgram[i].new_state=EMB_HOVER7; FlightProgram[i].ahead_angle = 0; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = EMB_HOVER7; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=ROTATE_RIGHT; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 0.7; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;
			//28
			//проходим последний поворот и летим вдоль стен
			FlightProgram[i].state = ROTATE_RIGHT; FlightProgram[i].condition_transition=ROTATE_COMPLETE; FlightProgram[i].new_state=MOVE_ALONG_LEFT_WALL; FlightProgram[i].ahead_angle = 270; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = MOVE_ALONG_LEFT_WALL; FlightProgram[i].condition_transition=OBSTACLE_AHEAD_FAR; FlightProgram[i].new_state=EMB_HOVER8; FlightProgram[i].ahead_angle = 270; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = EMB_HOVER8; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=ROTATE_RIGHT2; FlightProgram[i].ahead_angle = 270; FlightProgram[i].duration = 0.7; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = ROTATE_RIGHT2; FlightProgram[i].condition_transition=ROTATE_COMPLETE; FlightProgram[i].new_state=MOVE_ALONG_LEFT_WALL2; FlightProgram[i].ahead_angle = 180; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = MOVE_ALONG_LEFT_WALL2; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=LAND_BY_RANGES; FlightProgram[i].distance = 3; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].ahead_angle = 180; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = LAND_BY_RANGES; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=LAND_BY_RANGES2; FlightProgram[i].ahead_angle = 180; FlightProgram[i].distance = 20; FlightProgram[i].long_speed=longitudial_speed; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = LAND_BY_RANGES2; FlightProgram[i].condition_transition=LAND_RANGES; FlightProgram[i].new_state=EMB_HOVER9; FlightProgram[i].ahead_angle = 180; FlightProgram[i].action = NONE; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfFly; i++;

			FlightProgram[i].state = EMB_HOVER9; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=EMB_HOVER11; FlightProgram[i].ahead_angle = 180; FlightProgram[i].duration = 0.7; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = EMB_HOVER11; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=MOVE_ALONG_BACKWARD_WALL3; FlightProgram[i].ahead_angle = 180; FlightProgram[i].duration = 0.7; FlightProgram[i].action = CALC_DIST_BACK; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = MOVE_ALONG_BACKWARD_WALL3; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=EMB_HOVER12; FlightProgram[i].ahead_angle = 180; FlightProgram[i].distance = dist_backward; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = EMB_HOVER12; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LANDOFF; FlightProgram[i].ahead_angle = 180; FlightProgram[i].duration = 0.7; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;


			break;

		case 9: //AR.Drone is moving by square (for Carlos)
			maxCountElements = 26;
			FlightProgram = new ElementOfFlightProgram[maxCountElements];
			FlightProgram[i].state = WAIT_START; FlightProgram[i].condition_transition=START; FlightProgram[i].new_state=FLAT_TRIM; FlightProgram[i].action = NONE; FlightProgram[i].ahead_angle = 0;  i++;
			FlightProgram[i].state = FLAT_TRIM; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=TAKEOFF; FlightProgram[i].duration = 1.0; FlightProgram[i].action = FLATTRIM; FlightProgram[i].ahead_angle = 0; i++;
			FlightProgram[i].state = TAKEOFF; FlightProgram[i].condition_transition=HOVER; FlightProgram[i].new_state=EMB_HOVER; FlightProgram[i].action = NONE; FlightProgram[i].ahead_angle = 0; i++;
			if (useGyro == true){
				FlightProgram[i].state = EMB_HOVER; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; i++;
			}else{
				FlightProgram[i].state = EMB_HOVER; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 4.5; FlightProgram[i].action = CALIBRATE_COMPASS; i++;
			}
			FlightProgram[i].state = LIFTING; FlightProgram[i].condition_transition=HEIGHT_REACH; FlightProgram[i].new_state=EMB_HOVER2; FlightProgram[i].ahead_angle = 0; FlightProgram[i].action = SET_START_ANGLE; FlightProgram[i].long_speed=longitudial_speed;  FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = EMB_HOVER2; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=MOVE_FORWARD; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; FlightProgram[i].long_speed=longitudial_speed;  FlightProgram[i].height = HeightOfFly; i++;

            FlightProgram[i].state = MOVE_FORWARD; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=EMB_HOVER3; FlightProgram[i].ahead_angle = 0; FlightProgram[i].distance = 4; FlightProgram[i].long_speed=0.1;  FlightProgram[i].height = HeightOfFly; i++;
            FlightProgram[i].state = EMB_HOVER3; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=ROTATE_LEFT; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; FlightProgram[i].long_speed=02;  FlightProgram[i].height = HeightOfFly; i++;
            FlightProgram[i].state = ROTATE_LEFT; FlightProgram[i].condition_transition=ROTATE_COMPLETE; FlightProgram[i].new_state=EMB_HOVER4; FlightProgram[i].ahead_angle = 90; FlightProgram[i].long_speed=0.2;  FlightProgram[i].height = HeightOfFly; i++;
            FlightProgram[i].state = EMB_HOVER4; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=MOVE_FORWARD2; FlightProgram[i].ahead_angle = 90; FlightProgram[i].duration = 0.5; FlightProgram[i].action = NONE; FlightProgram[i].long_speed=02;  FlightProgram[i].height = HeightOfFly; i++;

            FlightProgram[i].state = MOVE_FORWARD2; FlightProgram[i].condition_transition=DISTANCE_Y; FlightProgram[i].new_state=EMB_HOVER5; FlightProgram[i].ahead_angle = 90; FlightProgram[i].distance = 4; FlightProgram[i].long_speed=0.1;  FlightProgram[i].height = HeightOfFly; i++;
            FlightProgram[i].state = EMB_HOVER5; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=ROTATE_LEFT2; FlightProgram[i].ahead_angle = 90; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; FlightProgram[i].long_speed=0.2;  FlightProgram[i].height = HeightOfFly; i++;
            FlightProgram[i].state = ROTATE_LEFT2; FlightProgram[i].condition_transition=ROTATE_COMPLETE; FlightProgram[i].new_state=EMB_HOVER6; FlightProgram[i].ahead_angle = 180; FlightProgram[i].long_speed=0.2;  FlightProgram[i].height = HeightOfFly; i++;
            FlightProgram[i].state = EMB_HOVER6; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=MOVE_FORWARD3; FlightProgram[i].ahead_angle = 180; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; FlightProgram[i].long_speed=0.2;  FlightProgram[i].height = HeightOfFly; i++;

            FlightProgram[i].state = MOVE_FORWARD3; FlightProgram[i].condition_transition=DISTANCE_X; FlightProgram[i].new_state=EMB_HOVER7; FlightProgram[i].ahead_angle = 180; FlightProgram[i].distance = 4; FlightProgram[i].long_speed=0.1;  FlightProgram[i].height = HeightOfFly; i++;
            FlightProgram[i].state = EMB_HOVER7; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=ROTATE_LEFT3; FlightProgram[i].ahead_angle = 180; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; FlightProgram[i].long_speed=02;  FlightProgram[i].height = HeightOfFly; i++;
            FlightProgram[i].state = ROTATE_LEFT3; FlightProgram[i].condition_transition=ROTATE_COMPLETE; FlightProgram[i].new_state=EMB_HOVER8; FlightProgram[i].ahead_angle = 270; FlightProgram[i].long_speed=0.2;  FlightProgram[i].height = HeightOfFly; i++;
            FlightProgram[i].state = EMB_HOVER8; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=MOVE_FORWARD4; FlightProgram[i].ahead_angle = 270; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; FlightProgram[i].long_speed=0.2;  FlightProgram[i].height = HeightOfFly; i++;

            FlightProgram[i].state = MOVE_FORWARD4; FlightProgram[i].condition_transition=DISTANCE_Y; FlightProgram[i].new_state=EMB_HOVER9; FlightProgram[i].ahead_angle = 270; FlightProgram[i].distance = 4; FlightProgram[i].long_speed=0.1;  FlightProgram[i].height = HeightOfFly; i++;
            FlightProgram[i].state = EMB_HOVER9; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=ROTATE_LEFT4; FlightProgram[i].ahead_angle = 270; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; FlightProgram[i].long_speed=0.2;  FlightProgram[i].height = HeightOfFly; i++;
            FlightProgram[i].state = ROTATE_LEFT4; FlightProgram[i].condition_transition=ROTATE_COMPLETE; FlightProgram[i].new_state=EMB_HOVER10; FlightProgram[i].ahead_angle = 0; FlightProgram[i].long_speed=0.2;  FlightProgram[i].height = HeightOfFly; i++;
            FlightProgram[i].state = EMB_HOVER10; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LANDOFF; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; FlightProgram[i].long_speed=01;  FlightProgram[i].height = HeightOfFly; i++;


			break;

		case 10: //Взлет, пролет вдоль 2-х стен, посадка на крест, взлет, разворот, пролет вдоль 2-х стен по дистанции
			maxCountElements = 8;
			FlightProgram = new ElementOfFlightProgram[maxCountElements];
			FlightProgram[i].state = WAIT_START; FlightProgram[i].condition_transition=START; FlightProgram[i].new_state=FLAT_TRIM; FlightProgram[i].action = NONE; FlightProgram[i].ahead_angle = 0; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = FLAT_TRIM; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=TAKEOFF; FlightProgram[i].duration = 1.0; FlightProgram[i].action = FLATTRIM; FlightProgram[i].ahead_angle = 0; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = TAKEOFF; FlightProgram[i].condition_transition=HOVER; FlightProgram[i].new_state=EMB_HOVER; FlightProgram[i].action = NONE; FlightProgram[i].ahead_angle = 0; FlightProgram[i].height = HeightOfFly; i++;
			if (useGyro == true){
				FlightProgram[i].state = EMB_HOVER; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;
			}else{
				FlightProgram[i].state = EMB_HOVER; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 4.5; FlightProgram[i].action = CALIBRATE_COMPASS; FlightProgram[i].height = HeightOfFly; i++;
			}
			FlightProgram[i].state = LIFTING; FlightProgram[i].condition_transition=HEIGHT_REACH; FlightProgram[i].new_state=MOVE_FORWARD; FlightProgram[i].ahead_angle = 0; FlightProgram[i].action = SET_START_ANGLE; FlightProgram[i].height = HeightOfFly; i++;
			//5
			FlightProgram[i].state = MOVE_FORWARD; FlightProgram[i].condition_transition=CROSS; FlightProgram[i].new_state=EMB_HOVER4; FlightProgram[i].ahead_angle = 0; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfCross; i++;
			FlightProgram[i].state = MOVE_FORWARD; FlightProgram[i].condition_transition=OBSTACLE_AHEAD; FlightProgram[i].new_state=EMB_HOVER2; FlightProgram[i].ahead_angle = 0; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfCross; i++;
			FlightProgram[i].state = EMB_HOVER2; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LANDOFF; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; FlightProgram[i].long_speed=longitudial_speed;  FlightProgram[i].height = HeightOfFly; i++;
			  break;

		case 11: //Взлет, пролет вдоль 2-х стен, посадка на крест
			maxCountElements = 8;
			FlightProgram = new ElementOfFlightProgram[maxCountElements];
			FlightProgram[i].state = WAIT_START; FlightProgram[i].condition_transition=START; FlightProgram[i].new_state=FLAT_TRIM; FlightProgram[i].action = NONE; FlightProgram[i].ahead_angle = 0; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = FLAT_TRIM; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=TAKEOFF; FlightProgram[i].duration = 1.0; FlightProgram[i].action = FLATTRIM; FlightProgram[i].ahead_angle = 0; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = TAKEOFF; FlightProgram[i].condition_transition=HOVER; FlightProgram[i].new_state=EMB_HOVER; FlightProgram[i].action = NONE; FlightProgram[i].ahead_angle = 0; FlightProgram[i].height = HeightOfFly; i++;
			if (useGyro == true){
				FlightProgram[i].state = EMB_HOVER; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;
			}else{
				FlightProgram[i].state = EMB_HOVER; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 4.5; FlightProgram[i].action = CALIBRATE_COMPASS; FlightProgram[i].height = HeightOfFly; i++;
			}
			FlightProgram[i].state = LIFTING; FlightProgram[i].condition_transition=HEIGHT_REACH; FlightProgram[i].new_state=MOVE_ALONG_2_WALL; FlightProgram[i].ahead_angle = 0; FlightProgram[i].action = SET_START_ANGLE; FlightProgram[i].height = HeightOfFly; i++;
			//5
			FlightProgram[i].state = MOVE_ALONG_2_WALL; FlightProgram[i].condition_transition=CROSS; FlightProgram[i].new_state=EMB_HOVER2; FlightProgram[i].ahead_angle = 0; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfCross; i++;
			FlightProgram[i].state = MOVE_ALONG_2_WALL; FlightProgram[i].condition_transition=OBSTACLE_AHEAD; FlightProgram[i].new_state=EMB_HOVER2; FlightProgram[i].ahead_angle = 0; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfCross; i++;
			FlightProgram[i].state = EMB_HOVER2; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LANDOFF; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; FlightProgram[i].long_speed=longitudial_speed;  FlightProgram[i].height = HeightOfFly; i++;
			  break;

		case 12: //Взлет, следование пути
			maxCountElements = 7;
			FlightProgram = new ElementOfFlightProgram[maxCountElements];
			FlightProgram[i].state = WAIT_START; FlightProgram[i].condition_transition=START; FlightProgram[i].new_state=FLAT_TRIM; FlightProgram[i].action = NONE; FlightProgram[i].ahead_angle = 0; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = FLAT_TRIM; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=TAKEOFF; FlightProgram[i].duration = 1.0; FlightProgram[i].action = FLATTRIM; FlightProgram[i].ahead_angle = 0; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = TAKEOFF; FlightProgram[i].condition_transition=HOVER; FlightProgram[i].new_state=EMB_HOVER; FlightProgram[i].action = NONE; FlightProgram[i].ahead_angle = 0; FlightProgram[i].height = HeightOfFly; i++;
			if (useGyro == true){
				FlightProgram[i].state = EMB_HOVER; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;
			}else{
				FlightProgram[i].state = EMB_HOVER; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 4.5; FlightProgram[i].action = CALIBRATE_COMPASS; FlightProgram[i].height = HeightOfFly; i++;
			}
			FlightProgram[i].state = LIFTING; FlightProgram[i].condition_transition=HEIGHT_REACH; FlightProgram[i].new_state=PATH_FOLLOW; FlightProgram[i].ahead_angle = 0; FlightProgram[i].action = SET_START_ANGLE; FlightProgram[i].height = HeightOfFly; i++;
			//5
			FlightProgram[i].state = PATH_FOLLOW; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=EMB_HOVER2; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 120.0; FlightProgram[i].long_speed=longitudial_speed_cross; FlightProgram[i].height = HeightOfCross; i++;
			FlightProgram[i].state = EMB_HOVER2; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LANDOFF; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; FlightProgram[i].long_speed=longitudial_speed;  FlightProgram[i].height = HeightOfFly; i++;
			  break;

		case 13: //Взлет, следование пунктирной линии
            maxCountElements = 7;
			FlightProgram = new ElementOfFlightProgram[maxCountElements];
            FlightProgram[i].state = WAIT_START; FlightProgram[i].condition_transition=START; FlightProgram[i].new_state=FLAT_TRIM; FlightProgram[i].action = NONE; FlightProgram[i].ahead_angle = 0; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = FLAT_TRIM; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=TAKEOFF; FlightProgram[i].duration = 1.0; FlightProgram[i].action = FLATTRIM; FlightProgram[i].ahead_angle = 0; FlightProgram[i].height = HeightOfFly; i++;
			FlightProgram[i].state = TAKEOFF; FlightProgram[i].condition_transition=HOVER; FlightProgram[i].new_state=EMB_HOVER; FlightProgram[i].action = NONE; FlightProgram[i].ahead_angle = 0; FlightProgram[i].height = HeightOfFly; i++;
			if (useGyro == true){
				FlightProgram[i].state = EMB_HOVER; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;
			}else{
				FlightProgram[i].state = EMB_HOVER; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 4.5; FlightProgram[i].action = CALIBRATE_COMPASS; FlightProgram[i].height = HeightOfFly; i++;
			}
			FlightProgram[i].state = LIFTING; FlightProgram[i].condition_transition=HEIGHT_REACH; FlightProgram[i].new_state=DASHLINE_FOLLOW; FlightProgram[i].ahead_angle = 0; FlightProgram[i].action = SET_START_ANGLE; FlightProgram[i].height = HeightOfFly; i++;
			//5
            FlightProgram[i].state = DASHLINE_FOLLOW; FlightProgram[i].condition_transition=LAND; FlightProgram[i].new_state=EMB_HOVER2; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 2000.0; FlightProgram[i].long_speed=longitudial_speed; FlightProgram[i].height = HeightOfCross; i++;
            FlightProgram[i].state = EMB_HOVER2; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=WAIT_START; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 5.0; FlightProgram[i].action = NONE; FlightProgram[i].long_speed=longitudial_speed;  FlightProgram[i].height = HeightOfFly; i++;
            //FlightProgram[i].state = LANDOFF; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=WAIT_START; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 20.0; FlightProgram[i].action = NONE; FlightProgram[i].long_speed=longitudial_speed;  FlightProgram[i].height = HeightOfFly; i++;
			  break;
        case 14:
            maxCountElements =7;
            FlightProgram = new ElementOfFlightProgram[maxCountElements];
            FlightProgram[i].state = WAIT_START; FlightProgram[i].condition_transition=START; FlightProgram[i].new_state=FLAT_TRIM; FlightProgram[i].action = NONE; FlightProgram[i].ahead_angle = 0; FlightProgram[i].height = HeightOfFly; i++;
            FlightProgram[i].state = FLAT_TRIM; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=TAKEOFF; FlightProgram[i].duration = 1.0; FlightProgram[i].action = FLATTRIM; FlightProgram[i].ahead_angle = 0; FlightProgram[i].height = HeightOfFly; i++;
            FlightProgram[i].state = TAKEOFF; FlightProgram[i].condition_transition=HOVER; FlightProgram[i].new_state=EMB_HOVER; FlightProgram[i].action = NONE; FlightProgram[i].ahead_angle = 0; FlightProgram[i].height = HeightOfFly; i++;
            if (useGyro == true){
                FlightProgram[i].state = EMB_HOVER; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 1.0; FlightProgram[i].action = NONE; FlightProgram[i].height = HeightOfFly; i++;
            }else{
                FlightProgram[i].state = EMB_HOVER; FlightProgram[i].condition_transition=TIME; FlightProgram[i].new_state=LIFTING; FlightProgram[i].ahead_angle = 0; FlightProgram[i].duration = 4.5; FlightProgram[i].action = CALIBRATE_COMPASS; FlightProgram[i].height = HeightOfFly; i++;
            }
            FlightProgram[i].state = LIFTING; FlightProgram[i].condition_transition=HEIGHT_REACH; FlightProgram[i].new_state=MOVE_FORWARD; FlightProgram[i].ahead_angle = 0; FlightProgram[i].action = SET_START_ANGLE; FlightProgram[i].height = HeightOfFly; i++;
            //5

            FlightProgram[i].state=MOVE_FORWARD;FlightProgram[i].condition_transition=DISTANCE_X;FlightProgram [i].new_state=EMB_HOVER2;FlightProgram [i].ahead_angle=0;FlightProgram [i].distance=2;FlightProgram[i].long_speed=0.15;FlightProgram[i].height=1;i++;
            FlightProgram[i].state=EMB_HOVER2;FlightProgram[i].condition_transition=TIME;FlightProgram[i].new_state=LANDOFF;FlightProgram[i].ahead_angle=0;FlightProgram[i].duration=2;FlightProgram[i].action=NONE;FlightProgram[i].height=1;i++;
            break;
        }


    }

    void ResetIntegErrors()
	{
		pid_Height->ResetIntState();
		pid_Direction->ResetIntState();
		pid_Lateral->ResetIntState();
		pid_CrossX->ResetIntState();
		pid_CrossY->ResetIntState();
		pid_PoseX->ResetIntState();
		pid_PoseY->ResetIntState();
	}

	void InitConditions()
	{
		ROS_INFO("Init conditions");
		Conditions[START] = false;
		Conditions[HEIGHT_REACH] = true;
		Conditions[FLOOR] = true;
		Conditions[CROSS] = true;
		Conditions[HOVER] = true;
		Conditions[OBSTACLE_AHEAD] = true;
		Conditions[OBSTACLE_AHEAD_FAR] = true;
		Conditions[ROTATE_COMPLETE] = true;
		Conditions[FREE_LEFT] = true;
		Conditions[FREE_RIGHT] = true;
		Conditions[LEFT_WALL] = true;
		Conditions[FRONT_WALL] = true;
		Conditions[FRONT_FREE] = true;
		Conditions[DISTANCE_X] = true;
		Conditions[DISTANCE_Y] = true;
		Conditions[BEFORE_CROSS] = true;
		Conditions[CENTER_OF_CROSS] = true;
		Conditions[TIME] = true;
		Conditions[LAND_X] = true;
		Conditions[LAND_RANGES] = true;
        Conditions[LAND] = true;
	}



	// Взлёт по cообщению
	void startCb(const std_msgs::EmptyConstPtr& msg)
	{
		initTime = ros::Time::now();
		Conditions[START] = true;
		ROS_INFO("Set condition START");
	}

    // Посадка по cообщению
    void LandCb(const std_msgs::EmptyConstPtr& msg)
    {
        Conditions[LAND] = true;
        ROS_INFO("Set condition LAND");
    }

	//проверка связи со смартфоном
	void checkCb(const std_msgs::EmptyConstPtr& msg)
	{
		ROS_INFO("Check connection");
	}

	// Публикация данных дальномеров, полученных по USB, в структуру
	void USBData(ardrone_autonomy::Ranges msg)
	{
		RangesDate_prev.r_front = RangesDate_cur.r_front;
		RangesDate_prev.r_back = RangesDate_cur.r_back;
		RangesDate_prev.r_left = RangesDate_cur.r_left;
		RangesDate_prev.r_right = RangesDate_cur.r_right;
		RangesDate_cur.r_front = msg.range_front;
		RangesDate_cur.r_back = msg.range_back;
		RangesDate_cur.r_left = msg.range_left;
		RangesDate_cur.r_right = msg.range_right;
	}

	// Посадка по кресту
	void crossCb(const visual_system::CrossConstPtr& msg)
	{

		x_cross=msg->x;
		y_cross=msg->y;
		lastObservCross=msg->header.stamp;

		if (Conditions[CROSS] == false)
		{
			//ROS_INFO("Cross");
			if (CountCross == 0)
			{
				prevCrossTime = ros::Time::now();
				CountCross++;
			}
			else
			{
				if ((ros::Time::now() - prevCrossTime).toSec() < 0.2)
				{
					CountCross++;
				}
				else
				{
					prevCrossTime = ros::Time::now();
					CountCross = 1;
				}
			}
			if (CountCross > 3){
				//запомним показания дальномеров и координаты креста на изображении
				offset_cross = x_cross*175;
				left_range_cross = RangesDate_cur.r_left + offset_cross;
				right_range_cross = RangesDate_cur.r_right - offset_cross;
				x_pose_cross = pose_.getOrigin().x();

				ROS_INFO("Set condition CROSS left_range=%.2f right_range=%.2f x_pose_cross=%.2f x_cross=%.2f", RangesDate_cur.r_left, RangesDate_cur.r_right, x_pose_cross, x_cross);
				Conditions[CROSS] = true;
				CountCross = 0;
			}
		}
		if (Conditions[BEFORE_CROSS] == false){


			if ((fabs(x_cross)<0.25) && ((ros::Time::now() - lastObservCross).toSec() < 0.15)) {

				if (CountCross == 0)
				{
					prevCrossTime = ros::Time::now();
					CountCross++;
				}
				else
				{
					if ((ros::Time::now() - prevCrossTime).toSec() < 0.3)
					{
						CountCross++;
					}
					else
					{
						prevCrossTime = ros::Time::now();
						CountCross = 1;
					}
				}
				if (CountCross > 2){
					x_land = pose_.getOrigin().x() + (-y_cross+1.5)*cos(target_angle);
					ROS_INFO("Set condition BEFORE__CROSS x_cross=%.2f y_cross=%.2f x_land=%.2f x_pose=%.2f",x_cross,y_cross, x_land, pose_.getOrigin().x());
					Conditions[BEFORE_CROSS] = true;
					CountCross = 0;
				}
			}
		}





	}

	void DashlineCb(const visual_system::DashlineConstPtr& msg)
	{
        if (msg->slope!=999)
		{
			dashline_slope = msg->slope;
			dashline_dist = msg->distance;
            dashline_straightPath = msg->straightPath;
			lastObservDashline=msg->header.stamp;
		}
	}


	void naviCb(ardrone_autonomy::Navdata msg)
	{
		LastNavDataMsg = msg;

		if (useGyro==true)
		{
			cur_angle = convertAngleARDroneToTf(LastNavMagMsg.heading_gyro_unwrapped) - startangle;
			//ROS_INFO("cur_angle = %.2f gyro = %.2f startangle =%.2f ", cur_angle, LastNavMagMsg.heading_gyro_unwrapped, startangle);
		}
		else
		{
			cur_angle = convertAngleARDroneToTf(LastNavDataMsg.rotZ) - startangle;
		}

		if(!prevNavDataTime.isZero()) dt = (msg.header.stamp - prevNavDataTime).toSec();
		prevNavDataTime = msg.header.stamp;
		if(startangle!=0) UpdatePose();

		PipeLineOfNavigation();
	}

	void navi_magCb(ardrone_autonomy::navdata_magneto msg)
	{
		LastNavMagMsg = msg;
	}

	void UpdatePose()
	{

		tf::Matrix3x3 attitude, yaw;
		attitude.setRPY(convertAngleARDroneToTf(LastNavDataMsg.rotX), convertAngleARDroneToTf(LastNavDataMsg.rotY), cur_angle);
		yaw.setRPY(0, 0, cur_angle);

		tf::Vector3 old_position = pose_.getOrigin();
		tf::Vector3 body_velocity(LastNavDataMsg.vx, LastNavDataMsg.vy, 0);

		// integrate velocity
		tf::Vector3 dposition = dt * body_velocity * 0.001;
		pose_.getOrigin() += yaw * dposition;

		pose_.setBasis(attitude);
		pose_.getOrigin().setZ(LastNavDataMsg.altd * 0.001);

		geometry_msgs::PoseStamped pose_msg;

		pose_msg.header.stamp = ros::Time::now();

		tf::poseTFToMsg(pose_, pose_msg.pose);

		pose_pub.publish(pose_msg);

	}

	/**
	 * AR.Drone angles are in degrees and in the interval [-180° +180°],
	 * in contrast, TF uses angles in radians and in the interval [0 2*pi]
	 */
	float convertAngleARDroneToTf(const float& angle)
	{
		return angle / 180.0f * M_PI + M_PI;
	}

	void PipeLineOfNavigation ()
	{
		EstimateConditions();
		EstimateNewState();
		EstimateControl();
		StabilizationSystem();

		if (DebugOutput && count>15){
			ROS_INFO("Front range = %.2f Left range = %.2f Right range = %.2f", RangesDate_cur.r_front, RangesDate_cur.r_left ,RangesDate_cur.r_right);
			ROS_INFO("Start angle=%.2f Current angle=%.2f Target angle=%.2f norm_diff_angle=%.2f", startangle, cur_angle, target_angle, norm_diff_angle);
			ROS_INFO("Navdata State=%d vx=%.2f vy=%.2f altd=%d rotZ=%.2f dt%.2f", state_nav, LastNavDataMsg.vx, LastNavDataMsg.vy, LastNavDataMsg.altd , LastNavDataMsg.rotZ, dt);
			ROS_INFO("Command l.x=%.2f l.y=%.2f l.z=%.2f a.x=%.2f a.y=%.2f a.z=%.2f", cmd_vel_msg.linear.x,cmd_vel_msg.linear.y,cmd_vel_msg.linear.z ,cmd_vel_msg.angular.x,cmd_vel_msg.angular.y, cmd_vel_msg.angular.z);
			ROS_INFO("Pose x=%.2f y=%.2f z=%.2f", pose_.getOrigin().x(), pose_.getOrigin().y(), pose_.getOrigin().z());
			ROS_INFO("Cross x=%.2f y=%.2f", x_cross, y_cross);
			count=0;
		}
		count++;
		PublishMessages();
	}

	void EstimateConditions()
	{
		//OBSTACLE_AHEAD
		if (RangesDate_cur.r_front<aheadDist && RangesDate_prev.r_front<1.1*aheadDist && Conditions[OBSTACLE_AHEAD] == false){
			ROS_INFO("Set condition OBSTACLE_AHEAD");
			Conditions[OBSTACLE_AHEAD] = true;
		}
		//OBSTACLE_AHEAD
		if (RangesDate_cur.r_front<aheadDistFar && RangesDate_prev.r_front<1.1*aheadDistFar && Conditions[OBSTACLE_AHEAD_FAR] == false){
			ROS_INFO("Set condition OBSTACLE_AHEAD_FAR");
			Conditions[OBSTACLE_AHEAD_FAR] = true;
		}
		//FREE_LEFT
		if (RangesDate_cur.r_left>freeDist && RangesDate_prev.r_left>freeDist && RangesDate_cur.r_front< aheadDistBeforeTurn && Conditions[FREE_LEFT] == false ){
			ROS_INFO("Set condition FREE_LEFT");
			Conditions[FREE_LEFT] = true;
		}
		
		//FREE_RIGHT
		if (RangesDate_cur.r_right>freeDist && RangesDate_prev.r_right>freeDist && RangesDate_cur.r_front< aheadDistBeforeTurn && Conditions[FREE_RIGHT] == false ){
			ROS_INFO("Set condition FREE_RIGHT");
			Conditions[FREE_RIGHT] = true;
		}

		//LAND_X
		if (fabs(x_land - pose_.getOrigin().x())<0.1 && Conditions[LAND_X] == false) {
			ROS_INFO("Set condition LAND_X x_land=%.2f cur_x=%.2f",x_land,pose_.getOrigin().x());
			Conditions[LAND_X] = true;
		}

		//LAND_RANGES
		if (RangesDate_cur.r_front<land_front && RangesDate_prev.r_front<1.1*land_front && Conditions[LAND_RANGES] == false) {
			ROS_INFO("Set condition LAND_RANGES r_front=%.2f",RangesDate_cur.r_front);
			Conditions[LAND_RANGES] = true;
		}

		//FRONT_WALL //добавлено для движения перед препятствием//
		if (RangesDate_cur.r_front<1.1*distToWall && RangesDate_prev.r_front<1.2*distToWall && Conditions[FRONT_WALL] == false){
			ROS_INFO("Set condition FRONT_WALL");
			Conditions[FRONT_WALL] = true;
		}
		
		//FRONT_FREE //добавлено для движения перед препятствием//
		if (RangesDate_cur.r_front>255 && RangesDate_prev.r_front>250 && (Types[state_nav]==tMOVE_ALONG_FORWARD_WALL)){
			ROS_INFO("Set condition FRONT_FREE");
			Conditions[FRONT_FREE] = true;
		}
		
		//LEFT_WALL
		if (RangesDate_cur.r_left<1.1*distToWall && RangesDate_prev.r_left<1.2*distToWall && Conditions[LEFT_WALL] == false){
			ROS_INFO("Set condition LEFT_WALL");
			Conditions[LEFT_WALL] = true;
		}
		//HEIGHT_REACH
		if (LastNavDataMsg.altd>cur_height  && Types[state_nav]==tLIFTING){
			ROS_INFO("Set condition HEIGHT_REACH altd=%d HeightofFly=%d", LastNavDataMsg.altd, HeightOfFly);
			Conditions[HEIGHT_REACH] = true;
		}
		//FLOOR
		if (LastNavDataMsg.altd<600  && Types[state_nav]==tDESCENT){
			ROS_INFO("Set condition FLOOR altd=%d", LastNavDataMsg.altd);
			Conditions[FLOOR] = true;
		}
		//HOVER
		if ((LastNavDataMsg.state==3 || LastNavDataMsg.state==7 || LastNavDataMsg.state==4) && Types[state_nav]==tTAKEOFF) {
			ROS_INFO("Set condition HOVER");
			Conditions[HOVER] = true;
		}
		//ROTATE_LEFT
		norm_diff_angle =atan2(sin(target_angle-cur_angle), cos(target_angle-cur_angle));
		if (fabs(norm_diff_angle)<0.05 && Conditions[ROTATE_COMPLETE] == false) {
			ROS_INFO("Set condition ROTATE_COMPLETE  cur_angle=%.2f target_angle=%.2f norm_diff_angle=%.2f",cur_angle, target_angle, norm_diff_angle);
			Conditions[ROTATE_COMPLETE] = true;
	
		}
		//DISTANCE
		if (fabs(x - pose_.getOrigin().x())>target_dist && Conditions[DISTANCE_X] == false) {
			ROS_INFO("Set condition DISTANCE_X x=%.2f cur_x=%.2f",x,pose_.getOrigin().x());
			Conditions[DISTANCE_X] = true;
		}
		if (fabs(y - pose_.getOrigin().y())>target_dist && Conditions[DISTANCE_Y] == false) {
			ROS_INFO("Set condition DISTANCE_Y y=%.2f cur_y=%.2f",y,pose_.getOrigin().y());
			Conditions[DISTANCE_Y] = true;
		}
		//CENTER_OF_CROSS
		if ((fabs(x_cross)<0.1) && (fabs(y_cross)<0.1) && ((ros::Time::now() - lastObservCross).toSec() < 0.1) && Types[state_nav]==tHOVER_OVER_CROSS) {
			ROS_INFO("Set condition CENTER_OF_CROSS x=%.2f y=%.2f",x_cross,y_cross);
			Conditions[CENTER_OF_CROSS] = true;
		}
		//TIME
		if ((ros::Time::now() - curTime).toSec() > target_time && Conditions[TIME] == false) {
			ROS_INFO("Set condition TIME");
			Conditions[TIME] = true;
		}

	}

	void EstimateNewState ()
	{
        StateNav new_state_nav = state_nav;
		for(int i=0; i<maxCountElements; i++)
		{
			if(state_nav==FlightProgram[i].state)
			{
				//if (state_nav==MOVE_FORWARD)
				//ROS_INFO("Conditions[CROSS]=%d Conditions[MOVE_FORWARD.condition_transition]=%d", Conditions[CROSS], Conditions[FlightProgram[i].condition_transition]);

				if (Conditions[FlightProgram[i].condition_transition])
				{
					new_state_nav = FlightProgram[i].new_state;
					ROS_INFO("Change state from %d to %d",state_nav, new_state_nav);
					x=pose_.getOrigin().x(); y=pose_.getOrigin().y();
					curTime = ros::Time::now();
					break;
				}
			}
		}

		//if state was changed then reset condition for new state
		if (new_state_nav != state_nav)
		{
			for(int i=0; i<maxCountElements; i++)
			{
				if(new_state_nav==FlightProgram[i].state)
				{
					Conditions[FlightProgram[i].condition_transition]=false;
					ResetIntegErrors(); //сбрасываем интегралы ошибок (особенно это важно для направления)
					target_angle = FlightProgram[i].ahead_angle/180.0f * M_PI;
					target_dist = FlightProgram[i].distance;
					target_time = FlightProgram[i].duration;
					cur_long_speed = FlightProgram[i].long_speed;
					cur_height = FlightProgram[i].height;
					switch (FlightProgram[i].action)
					{
					case TOGGLECAM:
						ToggleCam();
						break;
					case CALIBRATE_COMPASS:
						//startangle = convertAngleARDroneToTf(LastNavDataMsg.rotZ);
						//ROS_INFO("startangle=%.3f after calibrate compas", startangle);
						CalibrateCompas();
						break;
					case FLATTRIM:
						FlatTrim();
						break;
					case SET_START_ANGLE:
						if (useGyro==true)
						{
							startangle = convertAngleARDroneToTf(-0.001);//convertAngleARDroneToTf(LastNavMagMsg.heading_gyro_unwrapped);
							ROS_INFO("startangle=%.3f by Gyro", startangle);
						}
						else
						{
							startangle = convertAngleARDroneToTf(LastNavDataMsg.rotZ);
							ROS_INFO("startangle=%.3f by compass", startangle);
						}

						break;
					case CALC_X_CROSS_POSE:

						if (x_pose_cross == 0.0){
							x_pose_cross = pose_.getOrigin().x() - 5*cos(target_angle); //средняя точка зоны где может быть крест по оси Х в м
							ROS_INFO("x_pose_cross=%.3f x_pose=%.3f", x_pose_cross, pose_.getOrigin().x());
						}
						break;
					case CALC_DIST_BACK:
						dist_backward = (distToLand - RangesDate_cur.r_front)/100;
						FlightProgram[maxCountElements-2].distance = dist_backward;
						ROS_INFO("r_front=%.3f dist_backward = %.3f", RangesDate_cur.r_front, dist_backward);
						break;
					}
					ROS_INFO("Reset condition %d", FlightProgram[i].condition_transition);
				}
			}
		}
		state_nav = new_state_nav;
	}

	void EstimateControl()
	{
		switch (Types[state_nav])
		{
		case tLIFTING:
			cmd_vel_msg.linear.x = 0.0;
			cmd_vel_msg.linear.y = 0.0;
			cmd_vel_msg.linear.z = 0.8;
			cmd_vel_msg.angular.z = 0.0;
			break;
		case tDESCENT:
			cmd_vel_msg.linear.x = 0.0;
			cmd_vel_msg.linear.y = 0.0;
			cmd_vel_msg.linear.z = -0.6;
			cmd_vel_msg.angular.z = 0.0;
			break;
		case tTOUCH_FLOOR:
			cmd_vel_msg.linear.x = 0.0;
			cmd_vel_msg.linear.y = 0.0;
			cmd_vel_msg.linear.z = -1.0;
			cmd_vel_msg.angular.z = 0.0;
			break;
		case tKEEP_POSE:
			cmd_vel_msg.linear.x = 0.0;
			cmd_vel_msg.linear.y = 0.0;
			cmd_vel_msg.linear.z = 0.0;
			cmd_vel_msg.angular.z = 0.0;
			break;
		case tMOVE_FORWARD: case tMOVE_ALONG_LEFT_WALL: case tMOVE_ALONG_RIGHT_WALL: case tMOVE_ALONG_2_WALL: case tLAND_BY_RANGES:
			if (RangesDate_cur.r_front<50 && RangesDate_prev.r_front<50) {
				cmd_vel_msg.linear.x = 0.0;
			} else if (RangesDate_cur.r_front<200 && RangesDate_prev.r_front<220) {
				cmd_vel_msg.linear.x = 0.4*cur_long_speed;
			} else if (RangesDate_cur.r_front<350 && RangesDate_prev.r_front<370) {
				cmd_vel_msg.linear.x = 0.8*cur_long_speed;
			} else {
				cmd_vel_msg.linear.x = cur_long_speed;
			};
			cmd_vel_msg.linear.y = 0.0;
			cmd_vel_msg.linear.z = 0.0;
			cmd_vel_msg.angular.z = 0.0;
			break;
		case tMOVE_BACKWARD:
			cmd_vel_msg.linear.x = -longitudial_speed;
			cmd_vel_msg.linear.y = 0.0;
			cmd_vel_msg.linear.z = 0.0;
			cmd_vel_msg.angular.z = 0.0;
			break;
		case tMOVE_TO_LEFT_WALL:
			cmd_vel_msg.linear.x = longitudial_speed;
			cmd_vel_msg.linear.y = lateral_speed;
			cmd_vel_msg.linear.z = 0.0;
			cmd_vel_msg.angular.z = 0.0;
			break;
		case tMOVE_LEFT:
			cmd_vel_msg.linear.x = 0;
			cmd_vel_msg.linear.y = lateral_speed;
			cmd_vel_msg.linear.z = 0.0;
			cmd_vel_msg.angular.z = 0.0;
			break;
		case tMOVE_RIGHT: case tMOVE_ALONG_FORWARD_WALL://добавлено//
			cmd_vel_msg.linear.x = 0;
			cmd_vel_msg.linear.y = -lateral_speed;
			cmd_vel_msg.linear.z = 0.0;
			cmd_vel_msg.angular.z = 0.0;
			break;
		case tLANDOFF:
			cmd_vel_msg.linear.x = 0.0;
			cmd_vel_msg.linear.y = 0.0;
			cmd_vel_msg.linear.z = 0.0;
			cmd_vel_msg.angular.z = 0.0;
			break;
		case tHOVER:
			cmd_vel_msg.linear.x = 0;
			cmd_vel_msg.linear.y = 0;
			cmd_vel_msg.linear.z = 0;
			cmd_vel_msg.angular.z = 0;
			break;
		case tROTATE_LEFT:
			cmd_vel_msg.linear.x = 0.0;
			cmd_vel_msg.linear.y = 0.0;
			cmd_vel_msg.linear.z = 0.0;
			cmd_vel_msg.angular.z = angular_speed;
			break;
		case tROTATE_RIGHT:
			cmd_vel_msg.linear.x = 0.0;
			cmd_vel_msg.linear.y = 0.0;
			cmd_vel_msg.linear.z = 0.0;
			cmd_vel_msg.angular.z = -angular_speed;
			break;

		case tHOVER_OVER_CROSS:
			//Стабилизируемся по кресту только если сообщение не старое
			if ((ros::Time::now() - lastObservCross).toSec() < 0.1){
				cmd_vel_msg.linear.x = -pid_CrossX->UpdatePID(ros::Time::now(),y_cross, 0 ,y_cross,0);  //-Kp_Cross*y_cross;
				cmd_vel_msg.linear.y = -pid_CrossY->UpdatePID(ros::Time::now(),x_cross,0, x_cross,0);//-Kp_Cross*x_cross; //здесь минус потому что отрицательные значения y_cross показывают, что крест слева и чтобы лететь влево нам нужно положительное значение управления
			} else {
				cmd_vel_msg.linear.x = 0.0;
				cmd_vel_msg.linear.y = 0.0; //здесь минус потому что отрицательные значения y_cross показывают, что крест слева и чтобы лететь влево нам нужно положительное значение управления
			}
			cmd_vel_msg.linear.z = 0.0;
			cmd_vel_msg.angular.z = 0.0;
			break;
		case tHOVER_BEFORE_CROSS:
			//Стабилизируемся по кресту только если сообщение не старое
			if ((ros::Time::now() - lastObservCross).toSec() < 0.5){
				if (flagStab == true) {
					flagStab = false;
					ROS_INFO("Turn off stabilization.");
				}
				cmd_vel_msg.linear.x = 0.0;
				cmd_vel_msg.linear.y = -pid_CrossY->UpdatePID(ros::Time::now(),x_cross,0, x_cross,0);//-Kp_Cross*x_cross; //здесь минус потому что отрицательные значения y_cross показывают, что крест слева и чтобы лететь влево нам нужно положительное значение управления
			} else {
				cmd_vel_msg.linear.x = cur_long_speed_hover_cross;
				/*double err = (RangesDate_cur.r_left - RangesDate_cur.r_right) - (left_range_cross - right_range_cross);
				double cur_position = RangesDate_cur.r_left - RangesDate_cur.r_right;
				double des_value = left_range_cross - right_range_cross;*/

				double err = RangesDate_cur.r_left - RangesDate_cur.r_right;
				double cur_position = RangesDate_cur.r_left - RangesDate_cur.r_right;
				double des_value = 0;

				ros::Time t = ros::Time::now();

				//нужно сделать стабилизацию по дальномером иначе будет сносить

				if (useRangeForCross==true && flagStab==true){
					cmd_vel_msg.linear.y = pid_Lateral->UpdatePID(t, err, 0, cur_position, des_value); //если не видим крест, то стабилизируемся по дальномерам;
				} else {
					cmd_vel_msg.linear.y = 0;
				}

			}
			//изменяем направление движения если пролетели слишком много
			if ((pose_.getOrigin().x()+50) - (x_pose_cross+50)>3) {
				cur_long_speed_hover_cross = -long_speed_hover_cross*cos(target_angle);
				flagStab = true;
				ROS_INFO("Set stabilization. Change longitudial speed on %.2f", cur_long_speed_hover_cross);
			} else if ((pose_.getOrigin().x()+50) - (x_pose_cross+50)<-3) {
				cur_long_speed_hover_cross = long_speed_hover_cross*cos(target_angle);
				flagStab = true;
				ROS_INFO("Set stabilization. Change longitudial speed on %.2f", cur_long_speed_hover_cross);
			}

			if (RangesDate_cur.r_front<120 && RangesDate_prev.r_front<125 && cmd_vel_msg.linear.x>0){
				cur_long_speed_hover_cross =  -long_speed_hover_cross;
			}

			cmd_vel_msg.linear.z = 0.0;
			cmd_vel_msg.angular.z = 0.0;
			break;
		case tPATH_FOLLOW:
			pathFollower->calculateVelocityCommand(pose_, cmd_vel_msg);
			//ROS_INFO("Navigation: cmd_vel %f %f ==> %f", cmd_vel_msg.linear.x, cmd_vel_msg.linear.y, cmd_vel_msg.angular.z);
			break;
		case tDASHLINE_FOLLOW:

            double curDirection = isGazeboSimulation ? fabs(LastNavDataMsg.rotZ)-40.0:fabs(LastNavMagMsg.heading_gyro_unwrapped);
            //ROS_INFO("Choose speed: straight %s angle %f curDirection %f", dashline_straightPath ? "true" : "false", fabs(atan(dashline_slope)), curDirection);
            if (dashline_straightPath && (M_PI_2-fabs(atan(dashline_slope)))<0.1 && curDirection>100.0 && curDirection<120.0)
                cmd_vel_msg.linear.x = long_speed_along_straight_path;
            else
                cmd_vel_msg.linear.x = cur_long_speed;
			cmd_vel_msg.linear.y = 0.0;
			cmd_vel_msg.linear.z = 0.0;
			cmd_vel_msg.angular.z = 0.0;
			break;

		}

	}

	void StabilizationSystem ()
	{

		ros::Time t = ros::Time::now();

		double stab_pose_x = pid_PoseX->UpdatePID(t, x_land - pose_.getOrigin().x(), -LastNavDataMsg.vx/1000 ,pose_.getOrigin().x(), x_land);
		if (fabs(stab_pose_x)<0.01) stab_pose_x = 0.0;

		double stab_pose_y = pid_PoseY->UpdatePID(t, 0 - pose_.getOrigin().y(), -LastNavDataMsg.vy/1000 ,pose_.getOrigin().y(), 0);

		double stab_z = pid_Height->UpdatePID(t, cur_height - LastNavDataMsg.altd,0, LastNavDataMsg.altd, HeightOfFly);
		if (fabs(stab_z)<0.1) stab_z = 0.0;


		norm_diff_angle =atan2(sin(target_angle-cur_angle), cos(target_angle-cur_angle));
		double stab_rot_z = pid_Direction->UpdatePID(t, norm_diff_angle,0, cur_angle, target_angle);
		if (fabs(stab_rot_z)<0.01) stab_rot_z = 0.0;

		if (useGyro==true) stab_rot_z = -stab_rot_z;

		if (Types[state_nav]==tDASHLINE_FOLLOW)
		{
			double dashline_angle = atan(dashline_slope);
			if (dashline_angle<0) dashline_angle+=M_PI;
			double stab_rot_z = pid_Direction->UpdatePID(t, M_PI_2-dashline_angle,0, dashline_angle, M_PI_2);
			if (fabs(stab_rot_z)<0.01) stab_rot_z = 0.0;
			if (useGyro==true) stab_rot_z = -stab_rot_z;
            cmd_vel_msg.angular.z = isGazeboSimulation ? stab_rot_z:-stab_rot_z;

            if (isnan(dashline_dist)) dashline_dist=0;

            double stab_lat = pid_Lateral->UpdatePID(t, dashline_dist, -LastNavDataMsg.vy, dashline_dist, 0);

            lastDashlineDist = dashline_dist;

            if (dashline_angle<1.3 || dashline_angle>1.8){
                stab_lat=0;
                //cmd_vel_msg.linear.x=0;
            }

			cmd_vel_msg.linear.y = stab_lat;
			cmd_vel_msg.linear.z = stab_z;


            if (useHoverStabilization)
            {
                if (!modeHover && fabs(dashline_dist)<15 && fabs(LastNavDataMsg.vy)>100)
                {
                    modeHover = true;
                    startTimeHover = t;
                } else
                {
                    if ((ros::Time::now()-startTimeHover).toSec()>0.5)
                        modeHover = false;
                }

                if(modeHover)
                {
                    cmd_vel_msg.linear.x = 0.0;
                    cmd_vel_msg.linear.y = 0.0;
                    cmd_vel_msg.linear.z = 0.0;
                    cmd_vel_msg.angular.z = 0.0;
                }

            }

			//ROS_INFO("Dashline angle=%.2f Direction stabilization %.2f", dashline_angle, stab_rot_z);
			//ROS_INFO("dashline_dist=%.2f Lateral stabilization %.2f", dashline_dist, stab_lat);
		}


		double err=0;
		double cur_position=0;
		double des_value = distToWall;
		if (Types[state_nav]==tMOVE_ALONG_LEFT_WALL)
		{
			err = RangesDate_cur.r_left - distToWall;
			cur_position = RangesDate_cur.r_left;
		}
		if (Types[state_nav]==tMOVE_ALONG_RIGHT_WALL)
		{
			err = distToWall - RangesDate_cur.r_right;
			cur_position = RangesDate_cur.r_right;
		}
		if (Types[state_nav]==tMOVE_ALONG_FORWARD_WALL)
		{
			err = RangesDate_cur.r_front - distToWall;
			cur_position = RangesDate_cur.r_front;
		}
		if (Types[state_nav]==tMOVE_ALONG_2_WALL || Types[state_nav]==tMOVE_BACKWARD)
		{
			err = RangesDate_cur.r_left - RangesDate_cur.r_right;
			cur_position = RangesDate_cur.r_left - RangesDate_cur.r_right;
			des_value = 0;
		}
		if (Types[state_nav]==tLAND_BY_RANGES)
		{
			err = (RangesDate_cur.r_left - RangesDate_cur.r_right) - lateral_shift;
			cur_position = RangesDate_cur.r_left - RangesDate_cur.r_right;
			des_value = lateral_shift;
		}



        double stab_wall =0;// pid_Lateral->UpdatePID(t, err, 0, cur_position, des_value);


		switch (Types[state_nav])
		{
		//подумать про стабилизацию по дальномерам при наборе высоты (чтобы не сдувало)
		case tLIFTING:
			//cmd_vel_msg.linear.x = stab_pose_x*cos(target_angle);
			//cmd_vel_msg.linear.y = stab_pose_y;
			cmd_vel_msg.angular.z = stab_rot_z;
			break;
		case tDESCENT:
			cmd_vel_msg.linear.x = stab_pose_x*cos(target_angle);
			//cmd_vel_msg.linear.y = stab_pose_y;
			cmd_vel_msg.angular.z = stab_rot_z;
			break;
		case tKEEP_POSE:
			cmd_vel_msg.linear.x = stab_pose_x;
			cmd_vel_msg.linear.y = stab_pose_y;
			cmd_vel_msg.angular.z = stab_rot_z;
			break;
		case tMOVE_FORWARD:
			//cmd_vel_msg.linear.y = stab_y;
			//cmd_vel_msg.linear.z = stab_z;
			cmd_vel_msg.angular.z = stab_rot_z;
			break;
		case tMOVE_ALONG_LEFT_WALL: case tMOVE_ALONG_RIGHT_WALL: case tMOVE_ALONG_2_WALL: case tLAND_BY_RANGES: case tMOVE_BACKWARD:
			cmd_vel_msg.linear.y = stab_wall;
			cmd_vel_msg.angular.z = stab_rot_z;
			cmd_vel_msg.linear.z = stab_z;
			break;
		case tMOVE_TO_LEFT_WALL:
			cmd_vel_msg.linear.z = stab_z;
			cmd_vel_msg.angular.z = stab_rot_z;
			break;
		case tROTATE_LEFT: case tROTATE_RIGHT:
			//cmd_vel_msg.linear.z = stab_z;
			break;
		case tHOVER_BEFORE_CROSS:
			cmd_vel_msg.angular.z = stab_rot_z;
			break;
		case tMOVE_ALONG_FORWARD_WALL:
			cmd_vel_msg.linear.x = stab_wall;
			cmd_vel_msg.angular.z = stab_rot_z;
			break;
		}
	}

    void PublishMessages()
    {
        if (!manualControl)
        {
            switch (Types[state_nav])
            {
            case tTAKEOFF:
                takeoff_pub.publish(empty_msg);
                break;
            case tHOVER: case tLIFTING: case tMOVE_FORWARD: case tMOVE_ALONG_LEFT_WALL: case tMOVE_TO_LEFT_WALL: case tMOVE_ALONG_FORWARD_WALL: case tROTATE_LEFT: case tMOVE_BACKWARD: case tHOVER_OVER_CROSS:
            case tMOVE_ALONG_RIGHT_WALL: case tMOVE_LEFT: case tMOVE_RIGHT: case tKEEP_POSE: case tHOVER_BEFORE_CROSS: case tMOVE_ALONG_2_WALL: case tDESCENT: case tROTATE_RIGHT: case tTOUCH_FLOOR: case tLAND_BY_RANGES:
            case tPATH_FOLLOW: case tDASHLINE_FOLLOW:
                cmd_vel_msg.angular.x = 0;
                cmd_vel_msg.angular.y = 0;
                cmdvel_pub.publish(cmd_vel_msg);
                break;
            case tLANDOFF:
                //ROS_INFO("Lanf off");
                cmdvel_pub.publish(cmd_vel_msg);
                land_pub.publish(empty_msg);
                break;
            }
        } else
        {
            if ((ros::Time::now() - lastManualControl).toSec()>0.2)
                publishEmptyControl();
        }
    }

	void ToggleCam()
	{
		ROS_INFO("Toggle cam");
		client_tc.call(srv_tc);
	}

	void CalibrateCompas()
	{
		ROS_INFO("Calibrate compass");
		client_calib_compass.call(srv_tc);
	}

	void FlatTrim()
	{
		ROS_INFO("Flat trim");
		client_ft.call(srv_tc);
	}

    void manualControlCb(geometry_msgs::Twist msg)
    {
        ROS_INFO("Manual control");
        manualControl = true;
        lastManualControl = ros::Time::now();
        cmdvel_pub.publish(msg);
    }

    void autoControlCb(std_msgs::Empty msg)
    {
        ROS_INFO("Auto control");
        manualControl = false;
    }

    void publishEmptyControl()
    {
        geometry_msgs::Twist cmd_msg;
        cmd_msg.linear.x = 0.0;
        cmd_msg.linear.y = 0.0;
        cmd_msg.linear.z = 0.0;
        cmd_msg.angular.z = 0.0;
        cmd_msg.angular.x = 0.0;
        cmd_msg.angular.y = 0.0;
        cmdvel_pub.publish(cmd_msg);
    }

};

int main(int argc, char** argv)
{
	ros::init(argc, argv, "navigation");
	ArDroneNav ARNav;
	ros::spin();
	return 0;
}

#!/usr/bin/env python

import math
import os

import particlesfilter

PARTICLES_COUNT = 6  # Total number of particles
SENSORS_COUNT = 3  # Total number of sensors per particle
SENSORS_ANGLES = [0, math.pi/2, 3*math.pi/2]
MAX_SENSORS_RANGE = 15.00  # Proximity sensor's max range

WIDTH = 15  # Real-map width
HEIGHT = 4  # Real-map height
SCALE = 10  # Map scale (map_coords/scale = real_coords)

# from MapData import MAP_DATA
from pam2map import pam2map
MAP_DATA = pam2map("/home/alex/catkin_ws/src/ardroneum/particles_filter/map150x40.pgm")
NORD = math.pi / 2

PROX_SENS_UPDATE_TIME = 0.05
UPDATE_TIME_EXP_RATIO = 2



def main():
    print ("Start")
    particles_filter = particlesfilter.ParticlesFilter(
            PARTICLES_COUNT, SENSORS_COUNT, WIDTH, HEIGHT, MAP_DATA, SCALE)
    
    #test
    d = particles_filter.ObsMap.calc_range(0.0 , 0.0 , 0 , 15);
    print ("Angle 0 Distance=%f" % (d)); 
    
    d = particles_filter.ObsMap.calc_range(0.0 , 0.0 , math.pi / 2 , 15);
    print ("Angle 90 Distance=%f" % (d)); 
    
    d = particles_filter.ObsMap.calc_range(0.0 , 0.0 , math.pi , 15);
    print ("Angle 180 Distance=%f" % (d)); 
    
    d = particles_filter.ObsMap.calc_range(0.0 , 0.0 , 3*math.pi / 2 , 15);
    print ("Angle 270 Distance=%f" % (d)); 
    
    
    particles_filter.particles[0].x = 0.0
    particles_filter.particles[0].y = 0.0
    particles_filter.particles[0].h = 0.0
        
    particles_filter.particles[1].x = 5.0
    particles_filter.particles[1].y = 0.0
    particles_filter.particles[1].h = 0.0
        
    particles_filter.particles[2].x = -5.0
    particles_filter.particles[2].y = 0.0
    particles_filter.particles[2].h = 0.0
        
    particles_filter.particles[3].x = -3.0
    particles_filter.particles[3].y = 1.0
    particles_filter.particles[3].h = 0.0
    
    particles_filter.particles[4].x = 0.0
    particles_filter.particles[4].y = 0.0
    particles_filter.particles[4].h = math.pi / 2
    
    particles_filter.particles[5].x = 0.0
    particles_filter.particles[5].y = 0.0
    particles_filter.particles[5].h = 3*math.pi / 2
        
    for p in particles_filter.particles:
      print p
      
    particles_filter.particles_advance(1.0, 0.0, 1.0)  
    
    print("After move")
	
    for p in particles_filter.particles:
      print p  
    
    particles_filter.update_by_sensor(0, 0, 8, 15)
	
    print("After update")
	
    for p in particles_filter.particles:
      print p  
      
    
    
if __name__ == '__main__':
    main()
